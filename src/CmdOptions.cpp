#include <iomanip>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <windef.h>

#ifndef WIN32_LEAN_AND_MEAN
    #define WIN32_LEAN_AND_MEAN
#endif
#include <windows.h>

using namespace std;

namespace TrekWrapper {

// forward declarations
extern string getExePath();
extern string getExeName();
extern map<string_view, string_view> parseCommandLine(const char* lpCmdLine);
extern void openConsole();
extern void printToFile(const char* filePath);

// refer https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?f=59&t=2181
static const set<string_view> TrekArgs = {
    // direct play lobby calls (in upper case)
    "-C", "-c", "-H", "-h", "-L", "-i",
    // debug options
    "-BorgOff", "-Mudd", "-D", "-d", "-n",
    // test end game videos
    "-Bones", "-bones", "-Gorn", "-gorn", "-Kirk", "-kirk", "-Picard", "-picard",
};

using ParamMapping = map<string_view, string_view>;
using ParamGroups = map<string_view, ParamMapping>;

static const ParamGroups TrekArgDescs = {
    {
        "direct play lobby calls (in upper case)", {
            { "-C",                 "[loc_4025AA] setup multiplayer client" },  // same view as host
            { "-c",                 "[loc_4025AA] same like -C" },
            { "-H",                 "[loc_4026B6] setup multiplayer host" },    // same view as client
            { "-h",                 "[loc_4026B6] same like -H" },
            { "-L",                 "[asm_4026D8] join hosted game (SP setup locked)" },
            { "-i <ip-address>",    "[asm_4025B4] IP address specifier, used with -L" },
        }
    },
    {
        "debug options", {
            { "-BorgOff",           "[asm_402755] disable the random borg events" },
            { "-Mudd",              "[asm_4026FF] enable F10 / F11 cheat options" },
            { "-D",                 "[loc_402678] trigger breakpoint if launched by a debugger" },
            { "-d",                 "[loc_402678] same as -D" },
            { "-n",                 "[asm_402641] use stbof.ini STARTINGSEED number for new games" },
        }
    },
    {
        "test end game videos", {
            { "-Bones",             "[asm_402592] instant allied victory" },
            { "-bones",             "[asm_402592] same as -Bones" },
            { "-Gorn",              "[asm_4026A5] instant defeat" },
            { "-gorn",              "[asm_4026A5] same as -Gorn" },
            { "-Kirk",              "[asm_402622] instant final victory" },
            { "-kirk",              "[asm_402622] same as -Kirk" },
            { "-Picard",            "[asm_402667] instant domination victory" },
            { "-picard",            "[asm_402667] same as -Picard" },
        }
    }
};

// filtered trek command line arguments
static string trek_cmdArgsA;
static wstring trek_cmdArgsW;

static void updateConsoleOutput(map<string_view, string_view> cmdLine)
{
    if (cmdLine.find("-cmd") != cmdLine.end())
    {
        // open new console
        openConsole();
    }
    else
    {
        auto it = cmdLine.find("-f");
        if (it == cmdLine.end())
            it = cmdLine.find("-file");

        if (it != cmdLine.end() && !it->second.empty())
        {
            // print to log file
            string path = string(it->second);
            printToFile(path.c_str());
        }
    }
}

static void printHelp()
{
    // game.exe command line options
    cout << "Available " << getExeName() << " command line options:" << endl;
    cout << "    " << left << setw(25) << "-help:" << "print this help" << endl;
    cout << "    " << left << setw(25) << "-cmd:" << "open command prompt" << endl;
    cout << "    " << left << setw(25) << "-file <file-path>:" << "redirect console output to specified file" << endl;
    cout << "    " << left << setw(25) << "-f <file-path>:" << "shortcut to -file option" << endl;

    // trek.exe command line options
    cout << endl << "Available trek.exe command line options:" << endl;
    for (auto grp : TrekArgDescs)
    {
        cout << "  " << grp.first << ":" << endl;
        for (auto params : grp.second)
        {
            cout << "    " << left << setw(25) << string(params.first) + ":" << params.second << endl;
        }
    }
}

static void filterTrekCommands(map<string_view, string_view> commands)
{
    stringstream filtered;
    bool first = true;

    // filter trek.exe commands to fix evaluate the full command names
    for (auto cmd : commands)
    {
        if (TrekArgs.find(cmd.first) != TrekArgs.end())
        {
            if (first)
                first = false;
            else
                filtered << ' ';

            filtered << cmd.first << ' ' << cmd.second;
        }
    }

    trek_cmdArgsA = "\"" + getExePath() + "\" " + filtered.str();
    trek_cmdArgsW = wstring(trek_cmdArgsA.begin(), trek_cmdArgsA.end());
    cout << "filtered trek.exe arguments: " << trek_cmdArgsA << endl;
}

bool processCmdLine(const char* cmdLine)
{
    // parse command line
    auto cmds = parseCommandLine(cmdLine);
    if (cmds.size())
    {
        // first check how to print the console output
        updateConsoleOutput(cmds);

        // print help
        if (cmds.find("-help") != cmds.end())
        {
            openConsole();
            printHelp();
            return false;
        }

        // print arguments
        cout << "called with arguments:" << endl;
        for (auto cmd : cmds)
        {
            cout << "cmd: \"" << cmd.first << "\" arg: ";
            if (!cmd.second.empty())
                cout << "\"" << cmd.second << "\"";
            else
                cout << "none";
            cout << endl;
        }

        filterTrekCommands(cmds);
    }

    return true;
}

LPSTR Hook_GetCommandLineA()
{
    // return filtered trek command line arguments
    return const_cast<char*>(trek_cmdArgsA.c_str());
}

LPWSTR Hook_GetCommandLineW()
{
    // return filtered trek command line arguments
    return const_cast<wchar_t*>(trek_cmdArgsW.c_str());
}

} // namespace TrekWrapper
