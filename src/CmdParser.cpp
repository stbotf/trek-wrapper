#include <string>
#include <map>

using namespace std;

namespace TrekWrapper {

void parseCommandLine(map<string_view, string_view>& cmdList, const char* lpCmdLine)
{
    // check if empty
    if (lpCmdLine == nullptr || !lpCmdLine[0])
        return;

    string_view args = lpCmdLine;
    size_t cmd_pos = 0;

    while (true)
    {
        // find next spacing
        size_t sep = args.find_first_of(' ', cmd_pos + 1);
        if (sep == string::npos)
        {
            // single command with no spaces
            cmdList.emplace(args.substr(cmd_pos), args.end());
            break;
        }

        // parse command
        size_t cmd_len = sep - cmd_pos;
        string_view command = args.substr(cmd_pos, cmd_len);

        // parse command arguments
        size_t next = args.find(" -", sep);
        size_t prm_pos = sep + 1;
        if (next == string::npos)
        {
            // final arguments
            cmdList.emplace(command, args.substr(prm_pos));
            break;
        }

        string_view cmd_args = next != sep ? args.substr(prm_pos, next - prm_pos) : args.end();
        cmdList.emplace(command, cmd_args);
        cmd_pos = next + 1;
    }
}

map<string_view, string_view> parseCommandLine(const char* lpCmdLine)
{
    map<string_view, string_view> cmdList;
    parseCommandLine(cmdList, lpCmdLine);
    return cmdList;
}

} // namespace TrekWrapper
