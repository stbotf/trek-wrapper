#include "TrekWrapper/TrekSections.h"

#include <cstring>
#include <iostream>
#include <sstream>

#ifndef WIN32_LEAN_AND_MEAN
    #define WIN32_LEAN_AND_MEAN
#endif
#include <windows.h>

using namespace std;

namespace TrekWrapper {

static const int KeyCharacteristics = IMAGE_FILE_EXECUTABLE_IMAGE | IMAGE_FILE_32BIT_MACHINE;
static const size_t TrekEntryPoint = 0x54AC6A;

static bool validateDosHeader(const void* imageBase)
{
    const IMAGE_DOS_HEADER* dosHdr = (const IMAGE_DOS_HEADER*)imageBase;

    // validate DOS header image base address
    if (dosHdr->e_magic != IMAGE_DOS_SIGNATURE)
    {
        cout << "invalid DOS signature: 0x" << hex << dosHdr->e_magic << endl;
        return false;
    }

    // check PE header pointer
    if (dosHdr->e_lfanew != 0x80)
    {
        cout << "invalid PE header start: 0x" << hex << dosHdr->e_lfanew << endl;
        return false;
    }

    return true;
}

static bool validatePeHeader(const void* imageBase, bool self)
{
    // validate PE signature
    uint32_t peSignature = *((uint32_t*)((const size_t)imageBase + TrekPeHdrOffset));
    if (peSignature != IMAGE_NT_SIGNATURE)
    {
        cout << "invalid PE signature: 0x" << hex << peSignature << endl;
        return false;
    }

    IMAGE_FILE_HEADER* peHdr = (IMAGE_FILE_HEADER*)((const size_t)imageBase + TrekPeHdrOffset + 4);

    // validate machine type
    if (peHdr->Machine != IMAGE_FILE_MACHINE_I386)
    {
        cout << "invalid machine type: 0x" << hex << peHdr->Machine << endl;
        return false;
    }

    // validate characteristics
    if ((peHdr->Characteristics & KeyCharacteristics) != KeyCharacteristics)
    {
        cout << "invalid characteristics: 0x" << hex << peHdr->Characteristics << endl;
        return false;
    }

    // check whether the relocation table is stripped
    // for code embedding and mod compatibility the executable must not be relocatable
    if (!(peHdr->Characteristics & IMAGE_FILE_RELOCS_STRIPPED))
    {
        string err = "the relocation table is not stripped!";
        if (self)
        {
            cout << "Error: " << err << endl;
            return false;
        }

        cout << "Warning: " << err << endl;
    }

    // validate optional header size, which is required for executable images
    if (peHdr->SizeOfOptionalHeader != IMAGE_SIZEOF_NT_OPTIONAL32_HEADER)
    {
        cout << "invalid optional header size: " << peHdr->SizeOfOptionalHeader << endl;
        return false;
    }

    return true;
}

static bool validateOptionalHeader(const void* imageBase, bool self)
{
    IMAGE_OPTIONAL_HEADER32* optHdr = (IMAGE_OPTIONAL_HEADER32*)((const size_t)imageBase + TrekOptHdrOffset);

    // validate magic number for 32bit executables
    if (optHdr->Magic != IMAGE_NT_OPTIONAL_HDR32_MAGIC)
    {
        cout << "invalid optional header signature: 0x" << hex << optHdr->Magic << endl;
        return false;
    }

    // validate code base address
    if (self ? optHdr->BaseOfCode < TrekEndOffset : optHdr->BaseOfCode != TrekCodeOffset)
    {
        cout << "invalid code base address: 0x" << hex << optHdr->BaseOfCode << endl;
        return false;
    }

    // validate data base address
    if (self ? optHdr->BaseOfData < TrekEndOffset : optHdr->BaseOfData != TrekImportOffset)
    {
        cout << "invalid .idata base address: 0x" << hex << optHdr->BaseOfData << endl;
        return false;
    }

    // validate image base address
    if (optHdr->ImageBase != TrekBaseAddress)
    {
        cout << "invalid image base address: 0x" << hex << optHdr->ImageBase << endl;
        return false;
    }

    // validate import table address
    IMAGE_DATA_DIRECTORY& importDir = optHdr->DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT];
    if (self ? importDir.VirtualAddress < TrekEndOffset : importDir.VirtualAddress != TrekImportOffset)
    {
        cout << "invalid import table address: 0x" << hex << importDir.VirtualAddress << endl;
        if (self && importDir.VirtualAddress == TrekImportOffset)
            cout << "\nlooks like you corrupted the file header!" << endl;

        return false;
    }

    // validate resource table address
    IMAGE_DATA_DIRECTORY& resDir = optHdr->DataDirectory[IMAGE_DIRECTORY_ENTRY_RESOURCE];
    if (self ? resDir.VirtualAddress != 0 && resDir.VirtualAddress < TrekEndOffset
        : resDir.VirtualAddress != TrekRsrcOffset)
    {
        cout << "invalid resource table address: 0x" << hex << resDir.VirtualAddress << endl;
        if (self && resDir.VirtualAddress == TrekRsrcOffset)
            cout << "\nlooks like you corrupted the file header!" << endl;

        return false;
    }

    // validate base relocation table address
    IMAGE_DATA_DIRECTORY& relocDir = optHdr->DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC];
    if (relocDir.VirtualAddress != 0 || relocDir.Size != 0)
    {
        string err = "The base relocation table is still listed by the optional file header!";
        if (self)
        {
            cout << "Error: " << err << endl;
            return false;
        }

        cout << "Warning: " << err << endl;
    }

    return true;
}

static bool validateSectionHeader(const void* imageBase, string name, int idx,
    DWORD virtualAddress, DWORD sectionSize, DWORD characteristics, bool self)
{
    IMAGE_SECTION_HEADER* secHdr = (IMAGE_SECTION_HEADER*)((const size_t)imageBase + TrekSecHdrOffset + idx * IMAGE_SIZEOF_SECTION_HEADER);
    bool uninitialized = (characteristics & IMAGE_SCN_CNT_UNINITIALIZED_DATA) != 0;

    if (secHdr->VirtualAddress != virtualAddress)
    {
        cout << "invalid " << name << " virtual section address 0x" << hex << secHdr->VirtualAddress << endl;
        return false;
    }

    // for unmodded vanilla trek.exe the virtual size is not set
    if (secHdr->Misc.VirtualSize != 0 && secHdr->Misc.VirtualSize != sectionSize)
    {
        cout << "invalid " << name << " virtual section size 0x" << hex << secHdr->Misc.VirtualSize << endl;
        return false;
    }

    if (secHdr->SizeOfRawData != (self && uninitialized ? 0 : sectionSize))
    {
        cout << "invalid " << name << " raw data size 0x" << hex << secHdr->SizeOfRawData << endl;
        return false;
    }

    // for the debug build, skip to validate the raw pointer PointerToRawData
    if (secHdr->PointerToRelocations != 0)
    {
        stringstream err;
        err << name << " relocation pointer is still set to 0x" << hex << secHdr->PointerToRelocations;

        if (self)
        {
            cout << "Error: " << err.str() << endl;
            return false;
        }

        cout << "Warning: " << err.str() << endl;
    }

    if (secHdr->NumberOfRelocations != 0)
    {
        stringstream err;
        err << name << " relocation entry number is still set to: " << hex << secHdr->NumberOfRelocations;

        if (self)
        {
            cout << "Error: " << err.str() << endl;
            return false;
        }

        cout << "Warning: " << err.str() << endl;
    }


    if ((secHdr->Characteristics & characteristics) != characteristics)
    {
        cout << "invalid " << name << " characteristics: " << hex << secHdr->Characteristics << endl;
        return false;
    }

    return true;
}

static bool validateSectionHeaders(const void* imageBase, bool self)
{
    const IMAGE_FILE_HEADER& fileHeader = *((IMAGE_FILE_HEADER*)((const size_t)imageBase + 4));

    // .trekExe / .text (AUTO)
    static const int cx_trekExe = IMAGE_SCN_CNT_CODE|IMAGE_SCN_MEM_EXECUTE|IMAGE_SCN_MEM_READ;
    if (!validateSectionHeader(imageBase, self ? ".trekExe" : ".text", 0, TrekCodeOffset, TrekCodeSize, cx_trekExe, self))
        return false;

    // .trekImp / .idata
    static const int cx_trekImp = IMAGE_SCN_CNT_INITIALIZED_DATA|IMAGE_SCN_MEM_READ|IMAGE_SCN_MEM_WRITE;
    if (!validateSectionHeader(imageBase, self ? ".trekImp" : ".idata", 1, TrekImportOffset, TrekImportSize, cx_trekImp, self))
        return false;

    // .trekStr / .data (DGROUP)
    static const int cx_trekStr = IMAGE_SCN_CNT_INITIALIZED_DATA|IMAGE_SCN_MEM_READ|IMAGE_SCN_MEM_WRITE;
    if (!validateSectionHeader(imageBase, self ? ".trekStr" : ".data", 2, TrekStrOffset, TrekStrSize, cx_trekStr, self))
        return false;

    // .trekBss / .bss
    static const int cx_trekBss = IMAGE_SCN_CNT_UNINITIALIZED_DATA|IMAGE_SCN_MEM_READ|IMAGE_SCN_MEM_WRITE;
    if (!validateSectionHeader(imageBase, self ? ".trekBss" : ".bss", 3, TrekBssOffset, TrekBssSize, cx_trekBss, self))
        return false;

    if (self)
    {
        // .trekDat
        static const int cx_trekDat = IMAGE_SCN_CNT_INITIALIZED_DATA|IMAGE_SCN_MEM_READ;
        if (!validateSectionHeader(imageBase, ".trekDat", 4, TrekDataOffset, TrekDataSize, cx_trekDat, self))
            return false;
    }
    else
    {
        // .reloc
        static const int cx_trekReloc = IMAGE_SCN_CNT_INITIALIZED_DATA|IMAGE_SCN_MEM_READ;
        if (!validateSectionHeader(imageBase, ".reloc", 4, TrekRelocOffset, TrekRelocSize, cx_trekReloc, self))
            return false;

        // .rsrc
        static const int cx_trekRsrc = IMAGE_SCN_CNT_INITIALIZED_DATA|IMAGE_SCN_MEM_READ;
        if (!validateSectionHeader(imageBase, ".rsrc", 5, TrekRsrcOffset, TrekRsrcSize, cx_trekRsrc, self))
            return false;
    }

    // check remaining section headers
    for (int i = 5; i < fileHeader.NumberOfSections; ++i)
    {
        IMAGE_SECTION_HEADER* secHdr = (IMAGE_SECTION_HEADER*)(
            (const size_t)imageBase + TrekSecHdrOffset + i * IMAGE_SIZEOF_SECTION_HEADER);
        if (secHdr->VirtualAddress < TrekEndOffset)
        {
            cout << "section overlapping detected" << endl;
            return false;
        }
    }

    return true;
}

bool validateFileHeader(const void* imageBase, bool self)
{
    // validate DOS header
    if (!validateDosHeader(imageBase))
        return false;

    cout << "DOS header validated" << endl;

    // validate PE header
    if (!validatePeHeader(imageBase, self))
        return false;

    cout << "PE header validated" << endl;

    // validate required 'optional' header
    if (!validateOptionalHeader(imageBase, self))
        return false;

    cout << "optional header validated" << endl;

    // validate section headers
    if (!validateSectionHeaders(imageBase, self))
        return false;

    cout << "section headers validated" << endl;
    return true;
}

bool validateEntryPoint()
{
    uint64_t epStart = *((uint64_t*)TrekEntryPoint);
    bool valid = epStart == 0xEC83E58955525153;
    if (!valid)
        cout << "invalid entry point start: 0x" << hex << epStart << endl;

    return valid;
}

bool validate(const void* imageBase, bool self)
{
    // validate all the file headers
    if (!validateFileHeader(imageBase, self))
        return false;

    cout << "all file headers validated" << endl;

    // validate entry point
    if (!validateEntryPoint())
        return false;

    cout << "entry point validated" << endl;
    return true;
}

} // namespace TrekWrapper
