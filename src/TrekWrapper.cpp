#include "TrekWrapper/TrekSections.h"

#ifndef WIN32_LEAN_AND_MEAN
    #define WIN32_LEAN_AND_MEAN
#endif
#include <windows.h>
#include <fstream>
#include <iostream>
#include <string>

using namespace std;

namespace TrekWrapper {

// forward declarations
extern string getExePath();
extern string getExeDir();
extern bool fileExists(const string& path);
extern bool validateFileHeader(const void* imageBase, bool self);
extern bool validateEntryPoint();
extern bool validate(const void* imageBase, bool self);

static const unsigned int TREK_SIZE = 0x1B2E00;

static bool loadTrekSections()
{
    try
    {
        auto path = getExeDir();
        path += "trek.exe";

        if (!fileExists(path))
        {
            cout << "missing " << path << endl;
            return false;
        }

        cout << "attempting to load " << path << endl;

        // open file
        ifstream in(path.c_str(), ios::in|ios::binary);
        if (!in.is_open())
        {
            cout << "failed to load " << path << endl;
            return false;
        }

        // get file size
        in.seekg (0, in.end);
        auto fileSize = in.tellg();
        if (fileSize != TREK_SIZE)
        {
            cout << "invalid file size: " << fileSize << endl;
            return false;
        }

        cout << "file size: " << fileSize << endl;

        // seek back
        in.seekg(0, ios::beg);

        // load file header
        char* fileHeader = new char[TrekFileHeaderSize];
        in.read(fileHeader, TrekFileHeaderSize);
        if (!validateFileHeader(fileHeader, false))
            return false;

        cout << "all file headers validated" << endl;
        DWORD oldProtect;

        // read .trekExe section
        VirtualProtect((void*)TrekCodeAddress, TrekCodeSize, PAGE_READWRITE, &oldProtect);
        in.read((char*)TrekCodeAddress, TrekCodeSize);
        VirtualProtect((void*)TrekCodeAddress, TrekCodeSize, oldProtect, &oldProtect);

        // skip import section
        in.seekg(TrekFileStrAddress, ios::beg);

        // read .trekStr section
        VirtualProtect((void*)TrekStrAddress, TrekStrSize, PAGE_READWRITE, &oldProtect);
        in.read((char*)TrekStrAddress, TrekStrSize);
        VirtualProtect((void*)TrekStrAddress, TrekStrSize, oldProtect, &oldProtect);

        // read .trekDat section
        VirtualProtect((void*)TrekDataAddress, TrekDataSize, PAGE_READWRITE, &oldProtect);
        in.read((char*)TrekDataAddress, TrekDataSize);
        VirtualProtect((void*)TrekDataAddress, TrekDataSize, oldProtect, &oldProtect);

        in.close();
        cout << "sections loaded" << endl;
        if (!validateEntryPoint())
            return false;

        cout << "entry point validated" << endl;
        return true;
    }
    catch (const exception& e)
    {
        cout << "Error: " << e.what() << endl;
    }
    catch (...)
    {
        cout << "unknown load error encountered" << endl;
    }

    return false;
}

static void patchCode(void *dest, const char value, size_t n)
{
    /// make the memory page writable
    DWORD oldProtect;
    VirtualProtect(dest, n, PAGE_READWRITE, &oldProtect);

    /// apply the patch
    memset(dest, value, n);

    /// restore memory page protection
    VirtualProtect(dest, n, oldProtect, &oldProtect);
}

bool loadTrekContent()
{
    // validate the contained trek.exe data
    cout << "validating contained trek.exe sections..." << endl;
    if (!validate((void*)TrekBaseAddress, true) && !loadTrekSections())
    {
        cout << "no valid trek.exe file found" << endl;
        return false;
    }

    /// nop final botf exit call
    patchCode((void*)0x54AD08, 0x90, 5);

    return true;
}

} // namespace TrekWrapper
