#include "TrekWrapper/TrekImportTable.h"
#include "TrekWrapper/TrekSections.h"
#include "mpr.h"

#include <iostream>
#include <windef.h>

using namespace std;
using namespace MPR;

namespace TrekWrapper {

static inline void* getFncPtr(int address) { return *((void**)address); }
static const char*  prevDll  = nullptr;

static void patchImport(DllHook& hook)
{
    if (hook.dllName != prevDll) {
        cout << hook.dllName << ":" << endl;
        prevDll = hook.dllName;
    }

    cout << "  " << hook.fncName << ": " << hex << *hook.fncPtr << endl;
    *((const void**)hook.address) = *hook.fncPtr;
}

void clearImports()
{
    /// reset trek.exe import table
    memset((void*)TrekImportAddress, 0, TrekImportSize);
}

void patchImports()
{
    for (auto hook : HookList) {
        // skip trek extension updates
        if (getFncPtr(hook.address) != 0) {
            hook.modified = true;
            continue;
        }

        patchImport(hook);
    }
}

} // namespace TrekWrapper
