#ifndef WIN32_LEAN_AND_MEAN
    #define WIN32_LEAN_AND_MEAN
#endif
#include <windows.h>
#include <string>

using namespace std;

namespace TrekWrapper {

string getExePath()
{
    char path[MAX_PATH];
    int len = GetModuleFileNameA(NULL, path, MAX_PATH);
    return string(path, len);
}

string getExeName()
{
    string path = getExePath();
    size_t len = path.find_last_of("\\");
    return path.substr(len + 1);
}

string getExeDir()
{
    string path = getExePath();
    size_t len = path.find_last_of("\\");
    // include final back slashes
    return path.substr(0, len + 1);
}

bool fileExists(const string& path)
{
    DWORD attr = GetFileAttributesA(path.c_str());
    return (attr != INVALID_FILE_ATTRIBUTES && !(attr & FILE_ATTRIBUTE_DIRECTORY));
}

} // namespace TrekWrapper
