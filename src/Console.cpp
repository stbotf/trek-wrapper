/// according to:
/// Adding Console I/O to a Win32 GUI App
/// Windows Developer Journal, December 1997
/// http://dslweb.nwnexus.com/~ast/dload/guicon.htm

#ifndef WIN32_LEAN_AND_MEAN
	#define WIN32_LEAN_AND_MEAN
#endif
#include <windows.h>

#include <fcntl.h>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

namespace TrekWrapper {

/// maximum mumber of lines the output console should have
static const WORD MAX_CONSOLE_LINES = 10000;
static bool hasConsole = false;

void openConsole()
{
    if (hasConsole)
        return;

    /// allocate a console for this app
    AllocConsole();

    /// set the screen buffer to be big enough to let us scroll text
    CONSOLE_SCREEN_BUFFER_INFO coninfo;
    GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &coninfo);
    coninfo.dwSize.Y = MAX_CONSOLE_LINES;
    SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), coninfo.dwSize);

    /// redirect unbuffered STDOUT to the console
    long lStdHandle = (long)GetStdHandle(STD_OUTPUT_HANDLE);
    int hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
    FILE *fp = _fdopen( hConHandle, "w" );
    *stdout = *fp;
    /// disable buffer
    setvbuf( stdout, nullptr, _IONBF, 0 );

    /// redirect unbuffered STDIN to the console
    lStdHandle = (long)GetStdHandle(STD_INPUT_HANDLE);
    hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
    fp = _fdopen( hConHandle, "r" );
    *stdin = *fp;
    /// disable buffer
    setvbuf( stdin, nullptr, _IONBF, 0 );

    /// redirect unbuffered STDERR to the console
    lStdHandle = (long)GetStdHandle(STD_ERROR_HANDLE);
    hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
    fp = _fdopen( hConHandle, "w" );
    *stderr = *fp;
    /// disable buffer
    setvbuf( stderr, nullptr, _IONBF, 0 );

    /// by default cout/cin/cerr is synced with stdio
    /// it however needs to be refreshed
    ios::sync_with_stdio(false);
    ios::sync_with_stdio(true);
    hasConsole = true;
}

void printToFile(const char* filePath)
{
    /// redirect unbuffered STDOUT to the console
    FILE *fp = freopen(filePath, "w", stdout);
    *stdout = *fp;
    /// disable buffer
    setvbuf( stdout, nullptr, _IONBF, 0 );

    /// redirect unbuffered STDERR to the console
    *stderr = *fp;
    /// disable buffer
    setvbuf( stderr, nullptr, _IONBF, 0 );

    /// by default cout/cin/cerr is synced with stdio
    /// it however needs to be refreshed
    ios::sync_with_stdio(false);
    ios::sync_with_stdio(true);
}

} // namespace TrekWrapper
