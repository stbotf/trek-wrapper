#ifndef WIN32_LEAN_AND_MEAN
    #define WIN32_LEAN_AND_MEAN
#endif
#include <windows.h>
#include <iostream>

using namespace std;

// forward declarations
namespace TrekWrapper {
    extern bool processCmdLine(const char* cmdLine);
    extern bool loadTrekContent();
    extern bool clearImports();
    extern bool patchImports();
}

using namespace std;
using namespace TrekWrapper;

/// dummies to indicate section class, r/w/x permissions
/// and prevent empty sections being stripped
/// botf r/x code section
__attribute__((__section__(".trekExe"))) void sectionDummy_botfExe() {}
/// botf r/w import table
__attribute__((__section__(".trekImp"))) char sectionDummy_botfImp;
/// botf r/w string table
__attribute__((__section__(".trekStr"))) char sectionDummy_botfStr;
/// botf r/w bss section, zero intitialized
__attribute__((__section__(".trekBss"))) const char sectionDummy_botfBss = 0;
/// botf read only section, needs to be used
/// else the string gets stripped and it becomes r/w bss
/// content of this section is obsolete so section can be removed
__attribute__((__section__(".trekDat"))) const char sectionDummy_botfDat[] = "\0";
/// unused r/x code section for possible asm extensions
__attribute__((__section__(".extExe"))) void sectionDummy_extExe() {}
/// unused r/w memory section for possible asm extensions
__attribute__((__section__(".extMem"))) char sectionDummy_extMem;

static constexpr const char*  BotfExt_DllName = "trek-ext.dll";
static constexpr const char*  BotfExt_InitFnc = "init";
static void *const  TrekEntryPoint  = (void*) 0x54AC6A;

typedef void (*InitFncPtr)(LPSTR lpCmdLine, int nShowCmd);

static void loadBotfExt(LPSTR lpCmdLine, int nShowCmd) {
    /// load trek-ext.dll if available
    cout << "attempting to load " << BotfExt_DllName << "..." << endl;

    HINSTANCE hExtDll = LoadLibraryA(BotfExt_DllName);
    if (!hExtDll) {
        cout << "failed to load " << BotfExt_DllName << endl;
        return;
    }

    auto initFnc = (InitFncPtr*) GetProcAddress(hExtDll, BotfExt_InitFnc);
    if (!initFnc) {
        cout << "no " << BotfExt_InitFnc << " routine found" << endl;
        return;
    }

    (*initFnc)(lpCmdLine, nShowCmd);
    cout << "loaded " << BotfExt_DllName << endl;
}

int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
    // process command line arguments
    // when help is shown, pause and close the application without loading any additional data
    // if relevant to the trek-ext extension, we need to define some API
    if (!processCmdLine(lpCmdLine))
    {
        cout << endl;
        system("pause");
        return 0;
    }

    if (!loadTrekContent())
    {
        MessageBoxA(NULL, "Failed to load trek.exe file!", "Error", MB_ICONERROR|MB_OK);
        return 1;
    }

    /// zero trek.exe import table
    clearImports();

    /// load trek-ext.dll
    loadBotfExt(lpCmdLine, nShowCmd);

    /// load and patch all remaining dll imports
    cout << "patching remaining DLL imports..." << endl;
    patchImports();

    // pass execution to trek.exe entry point
    /// note that winmain requires some static pre-initialization,
    /// e.g. the thread local storage (ds:lpTlsValue) is initialized at asm_54A134
    cout << "passing execution to the trek.exe entry point" << endl;
    ((void(*)())TrekEntryPoint)();

    /// below actually never is executed, because on termination
    /// trek.exe calls the cstdlib exit call
    /// but reference botfDat so it is not removed
    cout << "execution completed" << sectionDummy_botfDat << endl;
    return 0;
}
