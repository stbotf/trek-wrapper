The Trek Wrapper is a container executable, built to hook, host and extend the BOTF trek.exe code.

[[_TOC_]]

For additional project details, refer:\
https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?f=10&t=3110

## Implementation

The wrapper launch process consists of the following steps:
1. the command line is evaluated for help and debug output
2. the own file sections are checked for corruption and for patched trek.exe content
3. if available and not already patched, the trek.exe file is validated and loaded into process space
4. if availabe it loads and initializes the trek-ext.dll library
5. then it updates all yet unpatched library routines from the original import table
6. and finally it passes code execution to the embedded trek.exe entry point

To allow embedding the code, the PE section layout is specifically re-assembled for the BOTF trek.exe executable.\
For any other applications, both the section layout and the application entry point must be modified.

The PE section layout is specified in a compiler dependent special GNU LD linker script.

For some weird reason, from my testing, all MinGW releases newer than MinGW GCC 8.1.0 break the iccvid.dll video rendering.\
When the video rendering is disabled, the game however also runs when compiled with GCC 12.2.0 and above.

To not enforce any specific GNU compiler on extension projects, and for not having to re-assemble the Trek Wrapper that many times, it is separated from the framework project but kept very basic.\
This way it can also easily be replaced by some alternative DLL injection approach.

Furthermore, to not mess with other components, the GCC and C++ standard libraries are statically linked.

## Advantages

By re-assembling a 'natural' executable, no external code patches are required, and the whole loading process can be controlled right from the start.

Specifically:
- it doesn't require a special launcher process to be run
- it doesn't mess the application folder with DLL replacements
- it avoids invasive code injections from another process
    - the former import section is replaced and not modified during runtime
    - all function mappings are updated in a normal writable data section
    - it is less likely to be false detected by anti virus hijacking heuristics
- it is ensured to be started ahead of any of the original trek.exe calls
- all the former static DLL sub-routine calls can individually be replaced by some own implementation
- it includes additional sections for assembler code extensions
    - one for executable code
    - and one for pre-initialized global variables (dynamic key variables)
- by not depending on any DLL injection techniques, it doesn't conflict with other DLL injection tools like DxWnd

## Build

To build the project, the very first you need is a working GCC MinGW 32bit compiler:
- the last known fully compatible compiler, that doesn't break the iccvid.dll video rendering, is the MinGW-W64 GCC-8.1.0 i686-win32-dwarf release from [https://sourceforge.net/projects/mingw-w64/files/](https://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win32/Personal%20Builds/mingw-builds/8.1.0/threads-win32/dwarf/i686-8.1.0-release-win32-dwarf-rt_v6-rev0.7z)
- cross compiling with x86_64 win32 SEH builds won't work, because the SEH exception model is still not implemented for 32bit\
see https://www.mingw-w64.org/contribute/#seh-for-32bits
- additionally, the x86_64 builds often miss the win32 32bit libraries, and therefore are useless for cross platform compilation

Then either:
- install Code::Blocks IDE from http://www.codeblocks.org/downloads/binaries/#imagesoswindows48pnglogo-microsoft-windows
    - on install select your default GCC MinGW C++ compiler
    - load the *.cbp project
    - if needed, update the project compiler
    - select release target and build
- install Visual Studio Code from https://code.visualstudio.com/
    - create a 'MINGW32' environment variable that points to your MinGW bin folder\
    ( i686-8.1.0-release-win32-dwarf-rt_v6-rev0\mingw32\bin )
    - open the project folder
    - accept to install the C/C++ Extension Pack
    - press ctrl+shift+B to build
- or configure and run a custom build on your own

## Usage

Once build, you might replace the marked wrapper exe section data that is filled with 0x11 to 0x55 with trek.exe content from 0x400 to file end at 0x1B2E00.

For the release build, the marked sections usually begin at 0x400 like with vanilla trek.exe.\
For the debug build, they usually start at 0x600, since there are more sections declared.

If not replaced, but placed along with trek.exe in the BOTF install folder, the wrapper on startup automatically validates and loads the missing code sections.

No additional files required.

## Troubleshooting

For execution, enable the 16bit color compatibility mode, or ensure that the file change date is old like 1999.\
Otherwise 16bit color resolutions will not be iterated and the graphics fail to initialize.

Alternatively, you might set the 'DWM8And16BitMitigation' registry flag, like it is described in:\
https://www.armadafleetcommand.com/onscreen/botf/viewtopic.php?f=303&t=4271

## Alternatives

There exist many alternative approaches, that allow to modify and extend the code.\
To gain full control, they however all require sophisticated code injection.

Such code injections on the one hand often are messy, and on the other hand they are more likely to be false detected by some anti-virus heuristics. Afterall these methods basically 'capture' the process and modify it on the fly, which also is a common technique to gain security access from another process.\
Finally, such code injections easily interfere with other code injections like the ones used by DxWnd.

One further alternative would be to enforce loading trek.exe as a library. But for mod compatibility it would require to ensure that the relocation table is removed so it becomes non-relocatable.
Additionally, to avoid memory conflicts, the process base address should be modified. And it should be the very first library that is loaded.

For further details refer:\
https://en.wikipedia.org/wiki/DLL_injection \
https://github.com/microsoft/Detours/wiki/DetourCreateProcessWithDllEx
