/// ---------------------------- mpr.h ----------------------------------
/// this file is an adjusted version of mpr.h from Falcon4 source

#ifndef MPR_H
#define MPR_H

#include <stddef.h>
#include <windef.h>

#include "mpr_enums.h"
#include "mpr_types.h"

#ifdef MPR_EXPORTS
    #define MPR_EXPORT __declspec(dllexport)
#else
    #define MPR_EXPORT __declspec(dllimport)
#endif

namespace MPR {

///----------------------------------------------------------------------
///MPRFunctions

#ifdef __cplusplus
extern "C" {
#endif

//! MPRMessage(dword, dword, void*, dword, dword)
MPR_EXPORT __stdcall uint32_t   MPRMessage(
    HResource           hRC,
    uint32_t            InDataSize,
    uint8_t*            InDataPtr,
    uint32_t            OutDataSize,
    uint8_t*            OutDataPtr);

//! MPRCreateRC(dword)
MPR_EXPORT __stdcall uint32_t   MPRCreateRC(
    HDevice             hDev);

//! MPRDeleteRC(void*)
MPR_EXPORT __stdcall uint32_t   MPRDeleteRC(
    HResource           hRC);

//! MPRNewTexture(dword, word, word, word, word)
MPR_EXPORT __stdcall uint32_t   MPRNewTexture(
    HResource           hRC,
    uint16_t            info,
    uint16_t            bits,
    uint16_t            width,
    uint16_t            height);

//! MPRLoadTexture(dword, dword, word, dword, dword, word, dword, dword, dword)
MPR_EXPORT __stdcall uint32_t   MPRLoadTexture(
    HResource           hRC,
    uint32_t            bitsPixel,
    uint16_t            mip,
    uint32_t            width,
    uint32_t            rows,
    int16_t             TexWidthBytes,
    uint32_t            id,
    uint32_t            chroma,
    uint8_t*            TexBuffer);

//! MPRFreeTexture(dword, dword)
MPR_EXPORT __stdcall void       MPRFreeTexture(
    HResource           hRC,
    uint32_t            id);

//! MPRSwapBuffers(void*, void*)
MPR_EXPORT __stdcall void       MPRSwapBuffers(
    HResource           hRC,
    uint32_t            lpSurface);

//! MPRStoreStateID(dword)
MPR_EXPORT __stdcall uint32_t   MPRStoreStateID(
    HResource           hRC);

//! MPRFreeStateID(dword, dword)
MPR_EXPORT __stdcall uint32_t   MPRFreeStateID(
    HResource           hRC,
    uint32_t            id);

//! MPRAttachSurface(dword, dword, dword)
MPR_EXPORT __stdcall uint32_t   MPRAttachSurface(
    HResource           hRC,
    uint32_t            lpDDSurface,
    uint32_t            info);

//! MPRNewTexturePalette(dword, word, word)
MPR_EXPORT __stdcall uint32_t   MPRNewTexturePalette(
    HResource           hRC,
    uint16_t            PalBitsPerEntry,
    uint16_t            PalNumEntries);

//! MPRLoadTexturePalette(dword, dword, word, word, word, dword, dword)
MPR_EXPORT __stdcall uint32_t   MPRLoadTexturePalette(
    HResource           hRC,
    uint32_t            id,
    uint16_t            info,
    uint16_t            PalBitsPerEntry,
    uint16_t            index,
    uint32_t            entries,
    uint8_t*            PalBuffer);

//! MPRFreeTexturePalette(void*, void*)
MPR_EXPORT __stdcall void       MPRFreeTexturePalette(
    HResource           hRC,
    uint32_t            id);

//! MPRAttachTexturePalette(dword, dword, dword)
MPR_EXPORT __stdcall void       MPRAttachTexturePalette(
    HResource           hRC,
    uint32_t            TexID,
    uint32_t            PalID);

//! MPRGetNumContexts is unused
MPR_EXPORT __stdcall uint32_t   MPRGetNumContexts(
    uint32_t            hDev);

//! MPROpenDevice(dword, dword, dword, dword)
MPR_EXPORT __stdcall uint32_t   MPROpenDevice(
    char*               drvName,
    uint32_t            devicenum,
    uint32_t            resnum,
    uint32_t            fullscreen);

//! MPRCloseDevice(void*)
MPR_EXPORT __stdcall int32_t    MPRCloseDevice(
    uint32_t            hDev);

//! MPRBlt(void*, void*, void*, dword)
MPR_EXPORT __cdecl uint32_t     MPRBlt(
    uint32_t            srcSurface,
    uint32_t            dstSurface,
    RECT*               srcRect,
    RECT*               dstRect);

//! MPRBltFill(void*, void*, void*, dword) not in mpr.h?
MPR_EXPORT __cdecl uint32_t     MPRBltFill(
    uint32_t            surface,
    RECT*               rect,
    uint32_t            colour,
    uint32_t            flags);

//! MPRBltTransparent(void*, void*, void*, dword)
MPR_EXPORT __cdecl uint32_t     MPRBltTransparent(
    uint32_t            srcSurface,
    uint32_t            dstSurface,
    RECT*               srcRect,
    RECT*               dstRect);

//! MPRCreateSurface(dword, dword, dword, dword, dword, dword, dword, dword)
MPR_EXPORT __cdecl uint32_t     MPRCreateSurface(
    uint32_t            DevID,
    uint32_t            width,
    uint32_t            height,
    BufferMode          front,
    BufferMode          back,
    HWND                win,
    uint32_t            clip,
    uint32_t            fullscreen);

//! MPRGetBackbuffer(void*)
MPR_EXPORT __cdecl uint32_t     MPRGetBackbuffer(
    uint32_t            handle);

//! MPRGetDeviceName(dword, dword, char*, dword)
MPR_EXPORT __cdecl uint32_t     MPRGetDeviceName(
    uint32_t            drivernum,
    uint32_t            devnum,
    char*               devicename,
    uint32_t            maxlen);

//! MPRGetDeviceResolution(dword, dword, dword, void*, void*, void*, void*, void* ,void*)
MPR_EXPORT __cdecl uint32_t     MPRGetDeviceResolution(
    uint32_t            drivernum,
    uint32_t            devnum,
    uint32_t            resnum,
    uint32_t*           width,
    uint32_t*           height,
    uint32_t*           RBitMask,
    uint32_t*           GBitMask,
    uint32_t*           BBitMask,
    uint32_t*           RGBBitCount);

//! MPRGetDriverName(void*, dword, dword)
MPR_EXPORT __cdecl uint32_t     MPRGetDriverName(
    uint32_t            drivernum,
    char*               drivername,
    uint32_t            maxlen);

//! MPRGetSurfaceDescription(void*, void*)
MPR_EXPORT __cdecl uint32_t     MPRGetSurfaceDescription(
    uint32_t            handle,
    SurfaceDesc*        surfdesc);

//! MPRInitialize(dword)
MPR_EXPORT __cdecl void         MPRInitialize(
    uint32_t            languageNum);

//! MPRLockSurface(void*)
MPR_EXPORT __cdecl uint32_t     MPRLockSurface(
    uint32_t            handle);

//! MPRReleaseSurface(void*)
MPR_EXPORT __cdecl uint32_t     MPRReleaseSurface(
    uint32_t            handle);

//! MPRRestoreSurface(void*)
MPR_EXPORT __cdecl uint32_t     MPRRestoreSurface(
    uint32_t            surface);

//! MPRSetChromaKey(void*, dword, dword)
MPR_EXPORT __cdecl uint32_t     MPRSetChromaKey(
    uint32_t            handle,
    uint32_t            rgba,
    uint32_t            colorkeybitsperpixel);

//! MPRTerminate()
MPR_EXPORT __cdecl void         MPRTerminate();

//! MPRUnlockSurface (seems to be similar to MPRLockSurface)
MPR_EXPORT __cdecl uint32_t     MPRUnlockSurface(
    uint32_t            handle);

#ifdef __cplusplus
}
#endif

} // namespace MPR

#endif // MPR_H
