/// ------------------------- mpr_enums.h -------------------------------
/// this file is an adjusted version of mpr.h from Falcon4 source

#ifndef MPR_ENUMS_H
#define MPR_ENUMS_H

#include <stdint.h>

namespace MPR {

namespace Enum_BufferMode
{
    enum BufferMode : uint32_t
    {
        /// Valid for front or back buffer specifier
        SystemMem = 0,
        VideoMem,
        /// Valid for front buffer specifier only
        Primary,
        /// Valid for back buffer specifier only
        Flip,
        None
    };
}
using Enum_BufferMode::BufferMode;

namespace Enum_Lang
{
    enum Lang : uint32_t
    {
        English = 0,
        German,
        Portuguese,
        Spanish,
        French,
        Italian,
        Japanese
    };
}
using Enum_Lang::Lang;

///----------------------------------------------------------------------
/// MPRSurface
namespace Enum_SurfaceBuffer
{
    enum SurfaceBuffer: uint32_t
    {
        /// draw buffer
        DrawBuffer      = 0x0080,
        /// z buffer
        DepthBuffer     = 0x0400,
        /// texture
        TextureBuffer   = 0x0800
    };
}
using Enum_SurfaceBuffer::SurfaceBuffer;

///----------------------------------------------------------------------
/// MPRMessage
namespace Enum_MsgID
{
    /// These are MPR message type definitions
    /// Pick ONE of ...
    enum MsgID: uint32_t
    {
        NOP = 0,
        CREATE_RC,
        DELETE_RC,
        GET_STATE,
        NEW_TEXTURE,
        LOAD_TEXTURE,
        FREE_TEXTURE,
        PACKET,
        BITMAP,
        NEW_TEXTURE_PAL,
        LOAD_TEXTURE_PAL,
        FREE_TEXTURE_PAL,
        ATTACH_TEXTURE_PAL,
        STORE_STATEID,
        GET_STATEID,
        FREE_STATEID,
        ATTACH_SURFACE,
        OPEN_DEVICE,
        CLOSE_DEVICE
    };
}
using Enum_MsgID::MsgID;

namespace Enum_PacketID
{
    /// These are the various packet identifers used by MPR.
    /// These are used to create general packets.
    /// These values are used in MPRMsgPtr_t.
    /// Pick ONE of ...
    enum PacketID: uint32_t
    {
        END = 0,                // MUST BE SAME AS HOOK_END
        SETSTATE,               // MUST BE SAME AS HOOK_SETSTATE
        RESTORESTATEID,         // MUST BE SAME AS HOOK_RESTORESTATEID
        STARTFRAME,             // MUST BE SAME AS HOOK_STARTFRAME
        FINISHFRAME,            // MUST BE SAME AS HOOK_FINISHFRAME
        CLEAR_BUFFERS,          // MUST BE SAME AS HOOK_CLEAR...
        SWAP_BUFFERS,           // MUST BE SAME AS HOOK_SWAP...
        POINTS,                 // MUST BE SAME AS HOOK_POINTS
        LINES,                  // MUST BE SAME AS HOOK_LINES
        POLYLINE,               // MUST BE SAME AS HOOK_POLYLINE
        TRIANGLES,              // MUST BE SAME AS HOOK_TRIANGLES
        TRISTRIP,               // MUST BE SAME AS HOOK_TRISTRIP
        TRIFAN,                 // MUST BE SAME AS HOOK_TRIFAN

        ID_COUNT,               // MUST BE LAST
    };
}
using Enum_PacketID::PacketID;

/// These are used by MPRGetState() and MPRSetState()
/// Pick ONE of ...
namespace Enum_RenderState
{
    /// Enumerated states.
    /// >> Possible values mentioned in the comments section.
    /// Pick ONE of ...
    enum RenderState : uint32_t
    {
        NONE = 0,
        ENABLES,                // use MPR_SE_...
        DISABLES,               // use MPR_SE_...

        SRC_BLEND_FUNCTION,     // use MPR_BF_...
        DST_BLEND_FUNCTION,     // use MPR_BF_...

        TEX_FILTER,             // use MPR_TX_...
        TEX_FUNCTION,           // use MPR_TF_...

        FG_COLOR,               // long, RGBA or index
        BG_COLOR,               // long, RGBA or index

        TEX_ID,                 // handle for current texture
    };
}
using Enum_RenderState::RenderState;

namespace Enum_SE_Settings
{
    /// Possible values for: MPR_STA_ENABLES, MPR_STA_DISABLES
    /// Logical OR of ...
    enum SE : uint32_t
    {
        SHADING         = 0x00000001L,  // interpolate r,g,b, a
        TEXTURING       = 0x00000002L,  // interpolate u,v,w
        MODULATION      = 0x00000004L,  // modulate color & texture
        Z_BUFFERING     = 0x00000008L,  // interpolate z and compare
        FOG             = 0x00000010L,  // interpolate fog
        PIXEL_MASKING   = 0x00000020L,  // selective pixel update

        Z_MASKING       = 0x00000040L,  // selective z update
        PATTERNING      = 0x00000080L,  // selective pixel & z update
        SCISSORING      = 0x00000100L,  // selective pixel & z update
        CLIPPING        = 0x00000200L,  // selective pixel & z update
        BLENDING        = 0x00000400L,  // per-pixel operation

        FILTERING       = 0x00002000L,  // texture filter control
        TRANSPARENCY    = 0x00004000L,  // texel use control

        NON_PERSPECTIVE_CORRECTION_MODE = 0x00200000L,
        NON_SUBPIXEL_PRECISION_MODE     = 0x00400000L,
        HARDWARE_OFF                    = 0x00800000L,
        PALETTIZED_TEXTURES             = 0x01000000L,
        DECAL_TEXTURES                  = 0x04000000L,
        ANTIALIASING                    = 0x08000000L,
        DITHERING                       = 0x10000000L
    };
}
using Enum_SE_Settings::SE;

namespace Enum_BF_Settings
{
    /// Possible values for: MPR_STA_SRC_BLEND_FUNCTION, MPR_STA_DST_BLEND_FUNCTION
    /// Pick one of ...
    enum BF : uint32_t
    {
        SRC_ALPHA = 0,
        SRC_ALPHA_INV

    // #if 1    // These are still referenced by the D3D driver.  Needs to be fixed...
    // //#if defined(MPR_BLEND_FUNCTIONS_ENABLED)
    //     MPR_BF_ZERO,
    //     MPR_BF_ONE,
    //     MPR_BF_SRC,             // only MPR_STA_DST_BLEND_FUNCTION
    //     MPR_BF_SRC_INV,         // only MPR_STA_DST_BLEND_FUNCTION
    //     MPR_BF_DST,             // only MPR_STA_SRC_BLEND_FUNCTION
    //     MPR_BF_DST_INV,         // only MPR_STA_SRC_BLEND_FUNCTION
    //     MPR_BF_DST_ALPHA,
    //     MPR_BF_DST_ALPHA_INV,
    // #endif
    };
}
using Enum_BF_Settings::BF;

namespace Enum_TX_Settings
{
    /// Possible values for: MPR_STA_TEX_FILTER
    /// Pick ONE of ...
    enum TX : uint32_t
    {
        NONE = 0,
        BILINEAR,              // interpolate 4 pixels
        DITHER,                // Dither the colors
        MIPMAP_NEAREST = 10,   // nearest mipmap
        MIPMAP_LINEAR,         // interpolate between mipmaps
        MIPMAP_BILINEAR,       // interpolate 4x within mipmap
        MIPMAP_TRILINEAR,      // interpolate mipmaps,4 pixels
    };
}
using Enum_TX_Settings::TX;

namespace Enum_TF_Settings
{
    /// Possible values for: MPR_STA_TEX_FUNCTION
    /// Pick ONE of ...
    enum TF : uint32_t
    {
        NONE = 0,
        FOO,
        MULTIPLY,
        SHADE,
        ALPHA,
        ADD,
    };
}
using Enum_TF_Settings::TF;

namespace Enum_VI_Settings
{
    /// Possible values for : VtxInfo in the MPRPrimitive() prototype below
    /// Logical OR of ...
    enum VI : uint32_t
    {
        COLOR       = 0x0002,
        TEXTURE     = 0x0004
    };
}
using Enum_VI_Settings::VI;

namespace Enum_TI_Settings
{
    /// Possible values for: TexInfo in the MPRNewTexture() prototype below.
    /// Logical OR of ...
    enum TI : uint32_t
    {
        DEFAULT     = 0x0000,
        MIPMAP      = 0x0001,
        CHROMAKEY   = 0x0020,
        ALPHA       = 0x0040,
        PALETTE     = 0x0080
    };
}
using Enum_TI_Settings::TI;

} // namespace MPR

#endif // MPR_ENUMS_H
