/// ------------------------- mpr_struct.h ------------------------------
/// this file is an adjusted version of mpr.h from Falcon4 source

#ifndef MPR_STRUCT_H
#define MPR_STRUCT_H

#include <stddef.h>
#include <windef.h>

#include "mpr_enums.h"

#ifdef MPR_EXPORTS
    #define MPR_EXPORT __declspec(dllexport)
#else
    #define MPR_EXPORT __declspec(dllimport)
#endif

namespace MPR {

/// MPR handles
typedef void* HDevice;
typedef void* HResource;

/** START OF SPECIAL "CARE" SECTION **/

/** MPR data types - Points
* New (reduced calorie) structures
* NOTE: Care is needed when mucking with these structures.
* Some of the asm code assumes groupings and alignments.*/
typedef struct {
    float x, y;
} Vtx_t;

typedef struct {
    float x, y;
    float r, g;
    float b, a;
#if defined(MP_DEPTH_BUFFER_ENABLED)
    float z;
#endif
} VtxClr_t;

typedef struct {
    float x, y;
    float r, g;
    float b, a;
    float u, v;
    float q;
#if defined(MPR_DEPTH_BUFFER_ENABLED)
    float z;
#endif
} VtxTexClr_t;

/** END OF SPECIAL "CARE" SECTION **/

/** MPRSurfaceDesc
* pitch is the actual size of each line of a texture within the memory
* the grafic driver can decide to share the memory or reuse available space
* therefore the memory might look like this:
* ##imgimgimg####
* ##imgimgimg####
* ##imgimgimg####
* ##imgimgimg####
* Stored in an array there will be no linebreaks of course, but accessing the
* data, getting a pointer to the beginning of the texture, the pitch has to be
* attended, which in this case would be the size of imgimgimg######
*/
struct SurfaceDesc
{
    uint32_t    height;
    uint32_t    width;
    uint32_t    pitch;
    uint32_t    RGBBitCount;
    uint32_t    RBitMask;
    uint32_t    GBitMask;
    uint32_t    BBitMask;
};

/// MPR data types - Message Header
struct MsgHdr_t{
    uint32_t    MsgHdrType;
    uint32_t    MsgHdrSize;
    HResource   MsgHdrRC;
};

// // MPR data types - Packet definition
// struct MPRMsgPkt_t{
//     MPRMsgHdr_t MsgPktHdr;
//     // variable size data
//     uint32_t    MsgPktBuffer[1];
// };

/// MPR data types - Packet (of primitives) definition
struct Pkt_t{
    uint32_t    PktType;
    // variable size data
    uint32_t    PktData[1];
};

/// MPR data types - Packet (of Ddraw Information) definition
struct PktDDraw_t{
    uint32_t    PktDDrawPktType;
    uint32_t    PktDDrawData0;
};

/// MPR data types - Packet (of State Information) definition
struct PktSta_t{
    uint32_t    PktStaPktType;
    uint32_t    PktStaState;
    uint32_t    PktStaData;
};

/// MPR data types - Packet (of primitives) definition
struct PktPrm_t{
    uint32_t    PktPrmPktType;
    uint32_t    PktPrmVtxNum;
    uint32_t    PktPrmVtxInfo;
    uint32_t    PktPrmVtxSize;
    // variable size data
    // uint32_t    PktPrmBuffer[1];
    uint8_t     PktPrmBuffer[1];
};

} // namespace MPR

#endif // MPR_STRUCT_H
