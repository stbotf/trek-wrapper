/**
 * Reassembled trek.exe mss.h 6.1a import header.
 */
#ifndef MSS_H
#define MSS_H

#include <stdint.h>
#include <windef.h>
#include <mmsystem.h>   // for HWAVEOUT & WAVEFORMAT

#ifdef MSS_EXPORTS
    #define MSS_EXPORT __declspec(dllexport)
#else
    #define MSS_EXPORT __declspec(dllimport)
#endif

// sample type format / PCM data formats
// masks
#define DIG_F_16BITS_MASK       1
#define DIG_F_STEREO_MASK       2
#define DIG_F_ADPCM_MASK        4
// formats
#define DIG_F_MONO_8            0
#define DIG_F_MONO_16           (DIG_F_16BITS_MASK)
#define DIG_F_STEREO_8          (DIG_F_STEREO_MASK)
#define DIG_F_STEREO_16         (DIG_F_STEREO_MASK|DIG_F_16BITS_MASK)
#define DIG_F_ADPCM_MONO_16     (DIG_F_ADPCM_MASK |DIG_F_16BITS_MASK)
#define DIG_F_ADPCM_STEREO_16   (DIG_F_ADPCM_MASK |DIG_F_16BITS_MASK|DIG_F_STEREO_MASK)
#define DIG_F_USING_ASI         16

// sample type flags / PCM flags
// obsolete
#define DIG_PCM_SIGN            0x0001
#define DIG_PCM_ORDER           0x0002
// used by driver hardware
#define DIG_PCM_POLARITY        0x0004
#define DIG_PCM_SPLIT           0x0008
#define DIG_BUFFER_SERVICE      0x0010
#define DIG_DUAL_DMA            0x0020
#define DIG_RECORDING_SUPPORTED 0x8000

// sample status
#define SMP_FREE                0x01 // Sample is available for allocation
#define SMP_DONE                0x02 // Sample has finished playing, or has never been started
#define SMP_PLAYING             0x04 // Sample is playing
#define SMP_STOPPED             0x08 // Sample has been stopped
#define SMP_PLAYINGBUTRELEASED  0x10 // Sample is playing, but digital handle has been temporarily released

// handles
typedef void *HSTREAM;
typedef void *HPROVIDER;
typedef void *HSAMPLE;
typedef void *HDIGDRIVER;

#ifdef __cplusplus
extern "C" {
#endif

// used by trek.exe
MSS_EXPORT WINAPI HSAMPLE       AIL_allocate_sample_handle(HDIGDRIVER dig);
MSS_EXPORT WINAPI void          AIL_end_sample(HSAMPLE S);
MSS_EXPORT WINAPI void          AIL_init_sample(HSAMPLE S);
MSS_EXPORT WINAPI void          AIL_load_sample_buffer(HSAMPLE S, uint32_t buff_num, void const *buffer, uint32_t len);
MSS_EXPORT WINAPI void*         AIL_mem_alloc_lock(uint32_t size);
MSS_EXPORT WINAPI void          AIL_mem_free_lock(void *ptr);
MSS_EXPORT WINAPI int32_t       AIL_minimum_sample_buffer_size(HDIGDRIVER dig, int32_t playback_rate, int32_t format);
MSS_EXPORT WINAPI uint32_t      AIL_ms_count();
MSS_EXPORT WINAPI void          AIL_release_sample_handle(HSAMPLE S);
MSS_EXPORT WINAPI void          AIL_resume_sample(HSAMPLE S);
MSS_EXPORT WINAPI int32_t       AIL_sample_buffer_ready(HSAMPLE S);
MSS_EXPORT WINAPI uint32_t      AIL_sample_status(HSAMPLE S);
MSS_EXPORT WINAPI int32_t       AIL_sample_volume(HSAMPLE S);
MSS_EXPORT WINAPI int32_t       AIL_set_sample_file(HSAMPLE S, void const *file_image, int32_t block);
MSS_EXPORT WINAPI void          AIL_set_sample_loop_count(HSAMPLE S, int32_t loop_count);
MSS_EXPORT WINAPI void          AIL_set_sample_pan(HSAMPLE S, int32_t pan);
MSS_EXPORT WINAPI void          AIL_set_sample_playback_rate(HSAMPLE S, int32_t playback_rate);
MSS_EXPORT WINAPI void          AIL_set_sample_position(HSAMPLE S, uint32_t pos);
// e.g. use format: DIG_F_MONO_16, flags: DIG_PCM_SIGN
MSS_EXPORT WINAPI void          AIL_set_sample_type(HSAMPLE S, int32_t format, uint32_t flags);
MSS_EXPORT WINAPI void          AIL_set_sample_volume(HSAMPLE S, int32_t volume);
MSS_EXPORT WINAPI void          AIL_shutdown();
MSS_EXPORT WINAPI void          AIL_start_sample(HSAMPLE S);
MSS_EXPORT WINAPI int32_t       AIL_startup();
MSS_EXPORT WINAPI void          AIL_stop_sample(HSAMPLE S);
MSS_EXPORT WINAPI int32_t       AIL_waveOutOpen(HDIGDRIVER *drvr, HWAVEOUT **lphWaveOut, int32_t wDeviceID, WAVEFORMAT *lpFormat);

// others
MSS_EXPORT WINAPI char*         AIL_last_error();

#ifdef __cplusplus
}
#endif

#endif // MSS_H
