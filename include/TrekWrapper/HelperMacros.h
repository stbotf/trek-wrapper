#ifndef TREKWRAPPER_HELPERMACROS_H
#define TREKWRAPPER_HELPERMACROS_H

// wrap CONCAT to first resolve the macro values
#define CONCAT_VALUES(x, y) x##y
#define CONCAT(x, y) CONCAT_VALUES(x, y)

// wrap STR to first resolve the macro values
#define STR_VALUE(x) #x
#define STR(x) STR_VALUE(x)

#define ADD_HEX_PREFIX(x) CONCAT(0x, x)

#endif // TREKWRAPPER_HELPERMACROS_H
