#ifndef TREKWRAPPER_LANGCODE_H
#define TREKWRAPPER_LANGCODE_H

#include "HelperMacros.h"

// refer
// https://learn.microsoft.com/en-us/windows/win32/menurc/versioninfo-resource#langID
#define LNG_CODE_NEUTRAL    0000 // Neutral
#define LNG_CODE_GERMAN     0407 // German
#define LNG_CODE_ENGLISH    0809 // U.K. English

#define LNG_ID_NEUTRAL      ADD_HEX_PREFIX(LNG_CODE_NEUTRAL)
#define LNG_ID_GERMAN       ADD_HEX_PREFIX(LNG_CODE_GERMAN)
#define LNG_ID_ENGLISH      ADD_HEX_PREFIX(LNG_CODE_ENGLISH)

#endif // TREKWRAPPER_LANGCODE_H
