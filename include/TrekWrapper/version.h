#ifndef TREKWRAPPER_VERSION_H
#define TREKWRAPPER_VERSION_H

#include "HelperMacros.h"
#include <winuser.h>

#define VER_FILE        1,0,4,0
#define VER_PRODUCT     1,0,4,0
#define VER_FILE_STR    "1.0.4"
#define VER_PRODUCT_STR "1.0.4"

#define VER_APPNAME     "Trek Wrapper"
#define VER_APPDESC     "Star Trek - The Next Generation: Birth of the Federation"
#define VER_COMPANY     "AFC"
#define VER_COPYRIGHT   "\xA92023" // ISO 8859-1 copyright sign [Alt+0169]
#define VER_FILENAME    "game.exe"
#define VER_COMMENT_EN  "Used to hook and extend the trek.exe application code."
#define VER_COMMENT_DE  "Zum Erweitern des trek.exe Anwendungscodes."

// application language
// the first 4 characters must match the language code
#define VER_LNG_CODE    STR(LNG_CODE_NEUTRAL) "04E4"
#define VER_LNG_ID      LNG_ID_NEUTRAL

#endif // TREKWRAPPER_VERSION_H
