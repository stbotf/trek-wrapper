#ifndef TREKWRAPPER_TREKIMPORTTABLE_H
#define TREKWRAPPER_TREKIMPORTTABLE_H

#include "mpr.h"
#include "mss.h"

// undefine UNICODE for GetEnvironmentStrings
#ifdef UNICODE
    #undef UNICODE
#endif // UNICODE

#include <cstdint>
#include <dplobby.h>
#include <objbase.h>
#include <vfw.h>
#include <windef.h>

using namespace std;
using namespace MPR;


namespace TrekWrapper {

extern LPSTR    Hook_GetCommandLineA();
extern LPWSTR   Hook_GetCommandLineW();

struct DllHook {
    const char*     dllName;
    const char*     fncName;
    const uint32_t  address;
    const void**    fncPtr;
    boolean         modified = false;

    DllHook(const char* dllName, const char* fncName, uint32_t address, const void** fncPtr)
        : dllName(dllName), fncName(fncName), address(address), fncPtr(fncPtr)
    {
    }
};

static const char* str_ole32 = "ole32.dll";
static const char* str_dplayx = "dplayx.dll";
static const char* str_winmm = "winmm.dll";
static const char* str_mss32 = "mss32.dll";
static const char* str_advapi32 = "advapi32.dll";
static const char* str_msvfw32 = "msvfw32.dll";
static const char* str_kernel32 = "kernel32.dll";
static const char* str_user32 = "user32.dll";
static const char* str_mpr97 = "mpr97.dll";

// To get a valid address reference, library function designators first must be bound to an lvalue.
// By taking static references, we in addition get one layer of indirection to the import jump table,
// which is optimized away when dynamically reffering a function after the static libraries are loaded.
// Without the indirection layer, however the import table updates for DxWnd cursor positioning would be missed.
static const void* FncPtr_CoCreateInstance                  = (void*) CoCreateInstance;
static const void* FncPtr_CoInitialize                      = (void*) CoInitialize;
static const void* FncPtr_CoUninitialize                    = (void*) CoUninitialize;
static const void* FncPtr_DirectPlayLobbyCreateA            = (void*) DirectPlayLobbyCreateA;
static const void* FncPtr_DirectPlayCreate                  = (void*) DirectPlayCreate;
static const void* FncPtr_timeBeginPeriod                   = (void*) timeBeginPeriod;
static const void* FncPtr_timeEndPeriod                     = (void*) timeEndPeriod;
static const void* FncPtr_timeGetDevCaps                    = (void*) timeGetDevCaps;
static const void* FncPtr_timeGetTime                       = (void*) timeGetTime;
static const void* FncPtr_AIL_allocate_sample_handle        = (void*) AIL_allocate_sample_handle;
static const void* FncPtr_AIL_end_sample                    = (void*) AIL_end_sample;
static const void* FncPtr_AIL_init_sample                   = (void*) AIL_init_sample;
static const void* FncPtr_AIL_load_sample_buffer            = (void*) AIL_load_sample_buffer;
static const void* FncPtr_AIL_mem_alloc_lock                = (void*) AIL_mem_alloc_lock;
static const void* FncPtr_AIL_mem_free_lock                 = (void*) AIL_mem_free_lock;
static const void* FncPtr_AIL_minimum_sample_buffer_size    = (void*) AIL_minimum_sample_buffer_size;
static const void* FncPtr_AIL_ms_count                      = (void*) AIL_ms_count;
static const void* FncPtr_AIL_release_sample_handle         = (void*) AIL_release_sample_handle;
static const void* FncPtr_AIL_resume_sample                 = (void*) AIL_resume_sample;
static const void* FncPtr_AIL_sample_buffer_ready           = (void*) AIL_sample_buffer_ready;
static const void* FncPtr_AIL_sample_status                 = (void*) AIL_sample_status;
static const void* FncPtr_AIL_sample_volume                 = (void*) AIL_sample_volume;
static const void* FncPtr_AIL_set_sample_file               = (void*) AIL_set_sample_file;
static const void* FncPtr_AIL_set_sample_loop_count         = (void*) AIL_set_sample_loop_count;
static const void* FncPtr_AIL_set_sample_pan                = (void*) AIL_set_sample_pan;
static const void* FncPtr_AIL_set_sample_playback_rate      = (void*) AIL_set_sample_playback_rate;
static const void* FncPtr_AIL_set_sample_position           = (void*) AIL_set_sample_position;
static const void* FncPtr_AIL_set_sample_type               = (void*) AIL_set_sample_type;
static const void* FncPtr_AIL_set_sample_volume             = (void*) AIL_set_sample_volume;
static const void* FncPtr_AIL_shutdown                      = (void*) AIL_shutdown;
static const void* FncPtr_AIL_start_sample                  = (void*) AIL_start_sample;
static const void* FncPtr_AIL_startup                       = (void*) AIL_startup;
static const void* FncPtr_AIL_stop_sample                   = (void*) AIL_stop_sample;
static const void* FncPtr_AIL_waveOutOpen                   = (void*) AIL_waveOutOpen;
static const void* FncPtr_RegCloseKey                       = (void*) RegCloseKey;
static const void* FncPtr_RegEnumValueA                     = (void*) RegEnumValueA;
static const void* FncPtr_RegOpenKeyExA                     = (void*) RegOpenKeyExA;
static const void* FncPtr_ICDecompress                      = (void*) ICDecompress;
static const void* FncPtr_ICLocate                          = (void*) ICLocate;
static const void* FncPtr_ICSendMessage                     = (void*) ICSendMessage;
static const void* FncPtr_CloseHandle                       = (void*) CloseHandle;
static const void* FncPtr_CreateEventA                      = (void*) CreateEventA;
static const void* FncPtr_CreateFileA                       = (void*) CreateFileA;
static const void* FncPtr_CreateProcessA                    = (void*) CreateProcessA;
static const void* FncPtr_CreateThread                      = (void*) CreateThread;
static const void* FncPtr_DeleteCriticalSection             = (void*) DeleteCriticalSection;
static const void* FncPtr_DeleteFileA                       = (void*) DeleteFileA;
static const void* FncPtr_DosDateTimeToFileTime             = (void*) DosDateTimeToFileTime;
static const void* FncPtr_EnterCriticalSection              = (void*) EnterCriticalSection;
static const void* FncPtr_ExitProcess                       = (void*) ExitProcess;
static const void* FncPtr_ExitThread                        = (void*) ExitThread;
static const void* FncPtr_FileTimeToDosDateTime             = (void*) FileTimeToDosDateTime;
static const void* FncPtr_FileTimeToLocalFileTime           = (void*) FileTimeToLocalFileTime;
static const void* FncPtr_FileTimeToSystemTime              = (void*) FileTimeToSystemTime;
static const void* FncPtr_FindClose                         = (void*) FindClose;
static const void* FncPtr_FindFirstFileA                    = (void*) FindFirstFileA;
static const void* FncPtr_FindNextFileA                     = (void*) FindNextFileA;
static const void* FncPtr_FlushFileBuffers                  = (void*) FlushFileBuffers;
static const void* FncPtr_GetACP                            = (void*) GetACP;
static const void* FncPtr_GetCPInfo                         = (void*) GetCPInfo;
static const void* FncPtr_GetCommandLineA                   = (void*) Hook_GetCommandLineA;
static const void* FncPtr_GetCommandLineW                   = (void*) Hook_GetCommandLineW;
static const void* FncPtr_GetConsoleMode                    = (void*) GetConsoleMode;
static const void* FncPtr_GetCurrentDirectoryA              = (void*) GetCurrentDirectoryA;
static const void* FncPtr_GetCurrentProcessId               = (void*) GetCurrentProcessId;
static const void* FncPtr_GetCurrentThreadId                = (void*) GetCurrentThreadId;
static const void* FncPtr_GetCurrentThread                  = (void*) GetCurrentThread;
static const void* FncPtr_GetDriveTypeA                     = (void*) GetDriveTypeA;
static const void* FncPtr_GetEnvironmentStrings             = (void*) GetEnvironmentStrings;
static const void* FncPtr_GetFileAttributesA                = (void*) GetFileAttributesA;
static const void* FncPtr_GetFileType                       = (void*) GetFileType;
static const void* FncPtr_GetFullPathNameA                  = (void*) GetFullPathNameA;
static const void* FncPtr_GetLastError                      = (void*) GetLastError;
static const void* FncPtr_GetLocalTime                      = (void*) GetLocalTime;
static const void* FncPtr_GetModuleFileNameA                = (void*) GetModuleFileNameA;
static const void* FncPtr_GetModuleFileNameW                = (void*) GetModuleFileNameW;
static const void* FncPtr_GetModuleHandleA                  = (void*) GetModuleHandleA;
static const void* FncPtr_GetOEMCP                          = (void*) GetOEMCP;
static const void* FncPtr_GetProcAddress                    = (void*) GetProcAddress;
static const void* FncPtr_GetStdHandle                      = (void*) GetStdHandle;
static const void* FncPtr_GetTimeZoneInformation            = (void*) GetTimeZoneInformation;
static const void* FncPtr_GetVersion                        = (void*) GetVersion;
static const void* FncPtr_GlobalMemoryStatus                = (void*) GlobalMemoryStatus;
static const void* FncPtr_InitializeCriticalSection         = (void*) InitializeCriticalSection;
static const void* FncPtr_IsBadCodePtr                      = (void*) IsBadCodePtr;
static const void* FncPtr_IsBadReadPtr                      = (void*) IsBadReadPtr;
static const void* FncPtr_IsBadWritePtr                     = (void*) IsBadWritePtr;
static const void* FncPtr_LeaveCriticalSection              = (void*) LeaveCriticalSection;
static const void* FncPtr_LoadLibraryA                      = (void*) LoadLibraryA;
static const void* FncPtr_LocalFileTimeToFileTime           = (void*) LocalFileTimeToFileTime;
static const void* FncPtr_MoveFileA                         = (void*) MoveFileA;
static const void* FncPtr_MultiByteToWideChar               = (void*) MultiByteToWideChar;
static const void* FncPtr_ReadConsoleInputA                 = (void*) ReadConsoleInputA;
static const void* FncPtr_ReadFile                          = (void*) ReadFile;
static const void* FncPtr_ResumeThread                      = (void*) ResumeThread;
static const void* FncPtr_SetConsoleCtrlHandler             = (void*) SetConsoleCtrlHandler;
static const void* FncPtr_SetConsoleMode                    = (void*) SetConsoleMode;
static const void* FncPtr_SetCurrentDirectoryA              = (void*) SetCurrentDirectoryA;
static const void* FncPtr_SetEnvironmentVariableA           = (void*) SetEnvironmentVariableA;
static const void* FncPtr_SetEnvironmentVariableW           = (void*) SetEnvironmentVariableW;
static const void* FncPtr_SetEvent                          = (void*) SetEvent;
static const void* FncPtr_SetFilePointer                    = (void*) SetFilePointer;
static const void* FncPtr_SetLastError                      = (void*) SetLastError;
static const void* FncPtr_SetStdHandle                      = (void*) SetStdHandle;
static const void* FncPtr_SetThreadPriority                 = (void*) SetThreadPriority;
static const void* FncPtr_SetUnhandledExceptionFilter       = (void*) SetUnhandledExceptionFilter;
static const void* FncPtr_Sleep                             = (void*) Sleep;
static const void* FncPtr_SuspendThread                     = (void*) SuspendThread;
static const void* FncPtr_TlsAlloc                          = (void*) TlsAlloc;
static const void* FncPtr_TlsFree                           = (void*) TlsFree;
static const void* FncPtr_TlsGetValue                       = (void*) TlsGetValue;
static const void* FncPtr_TlsSetValue                       = (void*) TlsSetValue;
static const void* FncPtr_UnhandledExceptionFilter          = (void*) UnhandledExceptionFilter;
static const void* FncPtr_VirtualAlloc                      = (void*) VirtualAlloc;
static const void* FncPtr_VirtualFree                       = (void*) VirtualFree;
static const void* FncPtr_VirtualLock                       = (void*) VirtualLock;
static const void* FncPtr_VirtualQuery                      = (void*) VirtualQuery;
static const void* FncPtr_WaitForSingleObject               = (void*) WaitForSingleObject;
static const void* FncPtr_WideCharToMultiByte               = (void*) WideCharToMultiByte;
static const void* FncPtr_WriteConsoleA                     = (void*) WriteConsoleA;
static const void* FncPtr_WriteFile                         = (void*) WriteFile;
static const void* FncPtr_BeginPaint                        = (void*) BeginPaint;
static const void* FncPtr_BringWindowToTop                  = (void*) BringWindowToTop;
static const void* FncPtr_CharUpperBuffA                    = (void*) CharUpperBuffA;
static const void* FncPtr_CreateWindowExA                   = (void*) CreateWindowExA;
static const void* FncPtr_DefWindowProcA                    = (void*) DefWindowProcA;
static const void* FncPtr_DestroyWindow                     = (void*) DestroyWindow;
static const void* FncPtr_DispatchMessageA                  = (void*) DispatchMessageA;
static const void* FncPtr_EndPaint                          = (void*) EndPaint;
static const void* FncPtr_FindWindowA                       = (void*) FindWindowA;
static const void* FncPtr_FlashWindow                       = (void*) FlashWindow;
static const void* FncPtr_GetAsyncKeyState                  = (void*) GetAsyncKeyState;
static const void* FncPtr_GetCursorPos                      = (void*) GetCursorPos;
static const void* FncPtr_GetForegroundWindow               = (void*) GetForegroundWindow;
static const void* FncPtr_GetMessageA                       = (void*) GetMessageA;
static const void* FncPtr_GetSystemMetrics                  = (void*) GetSystemMetrics;
static const void* FncPtr_IsIconic                          = (void*) IsIconic;
static const void* FncPtr_LoadCursorA                       = (void*) LoadCursorA;
static const void* FncPtr_LoadIconA                         = (void*) LoadIconA;
static const void* FncPtr_MessageBeep                       = (void*) MessageBeep;
static const void* FncPtr_MessageBoxA                       = (void*) MessageBoxA;
static const void* FncPtr_OffsetRect                        = (void*) OffsetRect;
static const void* FncPtr_PeekMessageA                      = (void*) PeekMessageA;
static const void* FncPtr_PostMessageA                      = (void*) PostMessageA;
static const void* FncPtr_PostQuitMessage                   = (void*) PostQuitMessage;
static const void* FncPtr_RegisterClassA                    = (void*) RegisterClassA;
static const void* FncPtr_RegisterWindowMessageA            = (void*) RegisterWindowMessageA;
static const void* FncPtr_SendMessageA                      = (void*) SendMessageA;
static const void* FncPtr_SetCursorPos                      = (void*) SetCursorPos;
static const void* FncPtr_SetFocus                          = (void*) SetFocus;
static const void* FncPtr_ShowCursor                        = (void*) ShowCursor;
static const void* FncPtr_ShowWindow                        = (void*) ShowWindow;
static const void* FncPtr_TranslateMessage                  = (void*) TranslateMessage;
static const void* FncPtr_WaitMessage                       = (void*) WaitMessage;
static const void* FncPtr_MPRFreeStateID                    = (void*) MPRFreeStateID;
static const void* FncPtr_MPRStoreStateID                   = (void*) MPRStoreStateID;
static const void* FncPtr_MPRMessage                        = (void*) MPRMessage;
static const void* FncPtr_MPRFreeTexture                    = (void*) MPRFreeTexture;
static const void* FncPtr_MPRAttachTexturePalette           = (void*) MPRAttachTexturePalette;
static const void* FncPtr_MPRLoadTexture                    = (void*) MPRLoadTexture;
static const void* FncPtr_MPRNewTexture                     = (void*) MPRNewTexture;
static const void* FncPtr_MPRFreeTexturePalette             = (void*) MPRFreeTexturePalette;
static const void* FncPtr_MPRLoadTexturePalette             = (void*) MPRLoadTexturePalette;
static const void* FncPtr_MPRNewTexturePalette              = (void*) MPRNewTexturePalette;
static const void* FncPtr_MPRAttachSurface                  = (void*) MPRAttachSurface;
static const void* FncPtr_MPRCreateRC                       = (void*) MPRCreateRC;
static const void* FncPtr_MPRCloseDevice                    = (void*) MPRCloseDevice;
static const void* FncPtr_MPRDeleteRC                       = (void*) MPRDeleteRC;
static const void* FncPtr_MPROpenDevice                     = (void*) MPROpenDevice;
static const void* FncPtr_MPRSwapBuffers                    = (void*) MPRSwapBuffers;
static const void* FncPtr_MPRBltFill                        = (void*) MPRBltFill;
static const void* FncPtr_MPRBltTransparent                 = (void*) MPRBltTransparent;
static const void* FncPtr_MPRBlt                            = (void*) MPRBlt;
static const void* FncPtr_MPRCreateSurface                  = (void*) MPRCreateSurface;
static const void* FncPtr_MPRGetBackbuffer                  = (void*) MPRGetBackbuffer;
static const void* FncPtr_MPRGetDeviceName                  = (void*) MPRGetDeviceName;
static const void* FncPtr_MPRGetDeviceResolution            = (void*) MPRGetDeviceResolution;
static const void* FncPtr_MPRGetDriverName                  = (void*) MPRGetDriverName;
static const void* FncPtr_MPRGetSurfaceDescription          = (void*) MPRGetSurfaceDescription;
static const void* FncPtr_MPRInitialize                     = (void*) MPRInitialize;
static const void* FncPtr_MPRLockSurface                    = (void*) MPRLockSurface;
static const void* FncPtr_MPRReleaseSurface                 = (void*) MPRReleaseSurface;
static const void* FncPtr_MPRRestoreSurface                 = (void*) MPRRestoreSurface;
static const void* FncPtr_MPRSetChromaKey                   = (void*) MPRSetChromaKey;
static const void* FncPtr_MPRTerminate                      = (void*) MPRTerminate;
static const void* FncPtr_MPRUnlockSurface                  = (void*) MPRUnlockSurface;

static const DllHook HookList[] {
    DllHook(str_ole32,      "CoCreateInstance",                     0x5743D4, &FncPtr_CoCreateInstance),
    DllHook(str_ole32,      "CoInitialize",                         0x5743D8, &FncPtr_CoInitialize),
    DllHook(str_ole32,      "CoUninitialize",                       0x5743DC, &FncPtr_CoUninitialize),
    DllHook(str_dplayx,     "DirectPlayLobbyCreateA",               0x5743E4, &FncPtr_DirectPlayLobbyCreateA),
    DllHook(str_dplayx,     "DirectPlayCreate",                     0x5743E8, &FncPtr_DirectPlayCreate),
    DllHook(str_winmm,      "timeBeginPeriod",                      0x5743F0, &FncPtr_timeBeginPeriod),
    DllHook(str_winmm,      "timeEndPeriod",                        0x5743F4, &FncPtr_timeEndPeriod),
    DllHook(str_winmm,      "timeGetDevCaps",                       0x5743F8, &FncPtr_timeGetDevCaps),
    DllHook(str_winmm,      "timeGetTime",                          0x5743FC, &FncPtr_timeGetTime),
    DllHook(str_mss32,      "_AIL_allocate_sample_handle@4",        0x574404, &FncPtr_AIL_allocate_sample_handle),
    DllHook(str_mss32,      "_AIL_end_sample@4",                    0x574408, &FncPtr_AIL_end_sample),
    DllHook(str_mss32,      "_AIL_init_sample@4",                   0x57440C, &FncPtr_AIL_init_sample),
    DllHook(str_mss32,      "_AIL_load_sample_buffer@16",           0x574410, &FncPtr_AIL_load_sample_buffer),
    DllHook(str_mss32,      "_AIL_mem_alloc_lock@4",                0x574414, &FncPtr_AIL_mem_alloc_lock),
    DllHook(str_mss32,      "_AIL_mem_free_lock@4",                 0x574418, &FncPtr_AIL_mem_free_lock),
    DllHook(str_mss32,      "_AIL_minimum_sample_buffer_size@12",   0x57441C, &FncPtr_AIL_minimum_sample_buffer_size),
    DllHook(str_mss32,      "_AIL_ms_count@0",                      0x574420, &FncPtr_AIL_ms_count),
    DllHook(str_mss32,      "_AIL_release_sample_handle@4",         0x574424, &FncPtr_AIL_release_sample_handle),
    DllHook(str_mss32,      "_AIL_resume_sample@4",                 0x574428, &FncPtr_AIL_resume_sample),
    DllHook(str_mss32,      "_AIL_sample_buffer_ready@4",           0x57442C, &FncPtr_AIL_sample_buffer_ready),
    DllHook(str_mss32,      "_AIL_sample_status@4",                 0x574430, &FncPtr_AIL_sample_status),
    DllHook(str_mss32,      "_AIL_sample_volume@4",                 0x574434, &FncPtr_AIL_sample_volume),
    DllHook(str_mss32,      "_AIL_set_sample_file@12",              0x574438, &FncPtr_AIL_set_sample_file),
    DllHook(str_mss32,      "_AIL_set_sample_loop_count@8",         0x57443C, &FncPtr_AIL_set_sample_loop_count),
    DllHook(str_mss32,      "_AIL_set_sample_pan@8",                0x574440, &FncPtr_AIL_set_sample_pan),
    DllHook(str_mss32,      "_AIL_set_sample_playback_rate@8",      0x574444, &FncPtr_AIL_set_sample_playback_rate),
    DllHook(str_mss32,      "_AIL_set_sample_position@8",           0x574448, &FncPtr_AIL_set_sample_position),
    DllHook(str_mss32,      "_AIL_set_sample_type@12",              0x57444C, &FncPtr_AIL_set_sample_type),
    DllHook(str_mss32,      "_AIL_set_sample_volume@8",             0x574450, &FncPtr_AIL_set_sample_volume),
    DllHook(str_mss32,      "_AIL_shutdown@0",                      0x574454, &FncPtr_AIL_shutdown),
    DllHook(str_mss32,      "_AIL_start_sample@4",                  0x574458, &FncPtr_AIL_start_sample),
    DllHook(str_mss32,      "_AIL_startup@0",                       0x57445C, &FncPtr_AIL_startup),
    DllHook(str_mss32,      "_AIL_stop_sample@4",                   0x574460, &FncPtr_AIL_stop_sample),
    DllHook(str_mss32,      "_AIL_waveOutOpen@16",                  0x574464, &FncPtr_AIL_waveOutOpen),
    DllHook(str_advapi32,   "RegCloseKey",                          0x57446C, &FncPtr_RegCloseKey),
    DllHook(str_advapi32,   "RegEnumValueA",                        0x574470, &FncPtr_RegEnumValueA),
    DllHook(str_advapi32,   "RegOpenKeyExA",                        0x574474, &FncPtr_RegOpenKeyExA),
    DllHook(str_msvfw32,    "ICDecompress",                         0x57447C, &FncPtr_ICDecompress),
    DllHook(str_msvfw32,    "ICLocate",                             0x574480, &FncPtr_ICLocate),
    DllHook(str_msvfw32,    "ICSendMessage",                        0x574484, &FncPtr_ICSendMessage),
    DllHook(str_kernel32,   "CloseHandle",                          0x57448C, &FncPtr_CloseHandle),
    DllHook(str_kernel32,   "CreateEventA",                         0x574490, &FncPtr_CreateEventA),
    DllHook(str_kernel32,   "CreateFileA",                          0x574494, &FncPtr_CreateFileA),
    DllHook(str_kernel32,   "CreateProcessA",                       0x574498, &FncPtr_CreateProcessA),
    DllHook(str_kernel32,   "CreateThread",                         0x57449C, &FncPtr_CreateThread),
    DllHook(str_kernel32,   "DeleteCriticalSection",                0x5744A0, &FncPtr_DeleteCriticalSection),
    DllHook(str_kernel32,   "DeleteFileA",                          0x5744A4, &FncPtr_DeleteFileA),
    DllHook(str_kernel32,   "DosDateTimeToFileTime",                0x5744A8, &FncPtr_DosDateTimeToFileTime),
    DllHook(str_kernel32,   "EnterCriticalSection",                 0x5744AC, &FncPtr_EnterCriticalSection),
    DllHook(str_kernel32,   "ExitProcess",                          0x5744B0, &FncPtr_ExitProcess),
    DllHook(str_kernel32,   "ExitThread",                           0x5744B4, &FncPtr_ExitThread),
    DllHook(str_kernel32,   "FileTimeToDosDateTime",                0x5744B8, &FncPtr_FileTimeToDosDateTime),
    DllHook(str_kernel32,   "FileTimeToLocalFileTime",              0x5744BC, &FncPtr_FileTimeToLocalFileTime),
    DllHook(str_kernel32,   "FileTimeToSystemTime",                 0x5744C0, &FncPtr_FileTimeToSystemTime),
    DllHook(str_kernel32,   "FindClose",                            0x5744C4, &FncPtr_FindClose),
    DllHook(str_kernel32,   "FindFirstFileA",                       0x5744C8, &FncPtr_FindFirstFileA),
    DllHook(str_kernel32,   "FindNextFileA",                        0x5744CC, &FncPtr_FindNextFileA),
    DllHook(str_kernel32,   "FlushFileBuffers",                     0x5744D0, &FncPtr_FlushFileBuffers),
    DllHook(str_kernel32,   "GetACP",                               0x5744D4, &FncPtr_GetACP),
    DllHook(str_kernel32,   "GetCPInfo",                            0x5744D8, &FncPtr_GetCPInfo),
    DllHook(str_kernel32,   "GetCommandLineA",                      0x5744DC, &FncPtr_GetCommandLineA),
    DllHook(str_kernel32,   "GetCommandLineW",                      0x5744E0, &FncPtr_GetCommandLineW),
    DllHook(str_kernel32,   "GetConsoleMode",                       0x5744E4, &FncPtr_GetConsoleMode),
    DllHook(str_kernel32,   "GetCurrentDirectoryA",                 0x5744E8, &FncPtr_GetCurrentDirectoryA),
    DllHook(str_kernel32,   "GetCurrentProcessId",                  0x5744EC, &FncPtr_GetCurrentProcessId),
    DllHook(str_kernel32,   "GetCurrentThreadId",                   0x5744F0, &FncPtr_GetCurrentThreadId),
    DllHook(str_kernel32,   "GetCurrentThread",                     0x5744F4, &FncPtr_GetCurrentThread),
    DllHook(str_kernel32,   "GetDriveTypeA",                        0x5744F8, &FncPtr_GetDriveTypeA),
    DllHook(str_kernel32,   "GetEnvironmentStrings",                0x5744FC, &FncPtr_GetEnvironmentStrings),
    DllHook(str_kernel32,   "GetFileAttributesA",                   0x574500, &FncPtr_GetFileAttributesA),
    DllHook(str_kernel32,   "GetFileType",                          0x574504, &FncPtr_GetFileType),
    DllHook(str_kernel32,   "GetFullPathNameA",                     0x574508, &FncPtr_GetFullPathNameA),
    DllHook(str_kernel32,   "GetLastError",                         0x57450C, &FncPtr_GetLastError),
    DllHook(str_kernel32,   "GetLocalTime",                         0x574510, &FncPtr_GetLocalTime),
    DllHook(str_kernel32,   "GetModuleFileNameA",                   0x574514, &FncPtr_GetModuleFileNameA),
    DllHook(str_kernel32,   "GetModuleFileNameW",                   0x574518, &FncPtr_GetModuleFileNameW),
    DllHook(str_kernel32,   "GetModuleHandleA",                     0x57451C, &FncPtr_GetModuleHandleA),
    DllHook(str_kernel32,   "GetOEMCP",                             0x574520, &FncPtr_GetOEMCP),
    DllHook(str_kernel32,   "GetProcAddress",                       0x574524, &FncPtr_GetProcAddress),
    DllHook(str_kernel32,   "GetStdHandle",                         0x574528, &FncPtr_GetStdHandle),
    DllHook(str_kernel32,   "GetTimeZoneInformation",               0x57452C, &FncPtr_GetTimeZoneInformation),
    DllHook(str_kernel32,   "GetVersion",                           0x574530, &FncPtr_GetVersion),
    DllHook(str_kernel32,   "GlobalMemoryStatus",                   0x574534, &FncPtr_GlobalMemoryStatus),
    DllHook(str_kernel32,   "InitializeCriticalSection",            0x574538, &FncPtr_InitializeCriticalSection),
    DllHook(str_kernel32,   "IsBadCodePtr",                         0x57453C, &FncPtr_IsBadCodePtr),
    DllHook(str_kernel32,   "IsBadReadPtr",                         0x574540, &FncPtr_IsBadReadPtr),
    DllHook(str_kernel32,   "IsBadWritePtr",                        0x574544, &FncPtr_IsBadWritePtr),
    DllHook(str_kernel32,   "LeaveCriticalSection",                 0x574548, &FncPtr_LeaveCriticalSection),
    DllHook(str_kernel32,   "LoadLibraryA",                         0x57454C, &FncPtr_LoadLibraryA),
    DllHook(str_kernel32,   "LocalFileTimeToFileTime",              0x574550, &FncPtr_LocalFileTimeToFileTime),
    DllHook(str_kernel32,   "MoveFileA",                            0x574554, &FncPtr_MoveFileA),
    DllHook(str_kernel32,   "MultiByteToWideChar",                  0x574558, &FncPtr_MultiByteToWideChar),
    DllHook(str_kernel32,   "ReadConsoleInputA",                    0x57455C, &FncPtr_ReadConsoleInputA),
    DllHook(str_kernel32,   "ReadFile",                             0x574560, &FncPtr_ReadFile),
    DllHook(str_kernel32,   "ResumeThread",                         0x574564, &FncPtr_ResumeThread),
    DllHook(str_kernel32,   "SetConsoleCtrlHandler",                0x574568, &FncPtr_SetConsoleCtrlHandler),
    DllHook(str_kernel32,   "SetConsoleMode",                       0x57456C, &FncPtr_SetConsoleMode),
    DllHook(str_kernel32,   "SetCurrentDirectoryA",                 0x574570, &FncPtr_SetCurrentDirectoryA),
    DllHook(str_kernel32,   "SetEnvironmentVariableA",              0x574574, &FncPtr_SetEnvironmentVariableA),
    DllHook(str_kernel32,   "SetEnvironmentVariableW",              0x574578, &FncPtr_SetEnvironmentVariableW),
    DllHook(str_kernel32,   "SetEvent",                             0x57457C, &FncPtr_SetEvent),
    DllHook(str_kernel32,   "SetFilePointer",                       0x574580, &FncPtr_SetFilePointer),
    DllHook(str_kernel32,   "SetLastError",                         0x574584, &FncPtr_SetLastError),
    DllHook(str_kernel32,   "SetStdHandle",                         0x574588, &FncPtr_SetStdHandle),
    DllHook(str_kernel32,   "SetThreadPriority",                    0x57458C, &FncPtr_SetThreadPriority),
    DllHook(str_kernel32,   "SetUnhandledExceptionFilter",          0x574590, &FncPtr_SetUnhandledExceptionFilter),
    DllHook(str_kernel32,   "Sleep",                                0x574594, &FncPtr_Sleep),
    DllHook(str_kernel32,   "SuspendThread",                        0x574598, &FncPtr_SuspendThread),
    DllHook(str_kernel32,   "TlsAlloc",                             0x57459C, &FncPtr_TlsAlloc),
    DllHook(str_kernel32,   "TlsFree",                              0x5745A0, &FncPtr_TlsFree),
    DllHook(str_kernel32,   "TlsGetValue",                          0x5745A4, &FncPtr_TlsGetValue),
    DllHook(str_kernel32,   "TlsSetValue",                          0x5745A8, &FncPtr_TlsSetValue),
    DllHook(str_kernel32,   "UnhandledExceptionFilter",             0x5745AC, &FncPtr_UnhandledExceptionFilter),
    DllHook(str_kernel32,   "VirtualAlloc",                         0x5745B0, &FncPtr_VirtualAlloc),
    DllHook(str_kernel32,   "VirtualFree",                          0x5745B4, &FncPtr_VirtualFree),
    DllHook(str_kernel32,   "VirtualLock",                          0x5745B8, &FncPtr_VirtualLock),
    DllHook(str_kernel32,   "VirtualQuery",                         0x5745BC, &FncPtr_VirtualQuery),
    DllHook(str_kernel32,   "WaitForSingleObject",                  0x5745C0, &FncPtr_WaitForSingleObject),
    DllHook(str_kernel32,   "WideCharToMultiByte",                  0x5745C4, &FncPtr_WideCharToMultiByte),
    DllHook(str_kernel32,   "WriteConsoleA",                        0x5745C8, &FncPtr_WriteConsoleA),
    DllHook(str_kernel32,   "WriteFile",                            0x5745CC, &FncPtr_WriteFile),
    DllHook(str_user32,     "BeginPaint",                           0x5745D4, &FncPtr_BeginPaint),
    DllHook(str_user32,     "BringWindowToTop",                     0x5745D8, &FncPtr_BringWindowToTop),
    DllHook(str_user32,     "CharUpperBuffA",                       0x5745DC, &FncPtr_CharUpperBuffA),
    DllHook(str_user32,     "CreateWindowExA",                      0x5745E0, &FncPtr_CreateWindowExA),
    DllHook(str_user32,     "DefWindowProcA",                       0x5745E4, &FncPtr_DefWindowProcA),
    DllHook(str_user32,     "DestroyWindow",                        0x5745E8, &FncPtr_DestroyWindow),
    DllHook(str_user32,     "DispatchMessageA",                     0x5745EC, &FncPtr_DispatchMessageA),
    DllHook(str_user32,     "EndPaint",                             0x5745F0, &FncPtr_EndPaint),
    DllHook(str_user32,     "FindWindowA",                          0x5745F4, &FncPtr_FindWindowA),
    DllHook(str_user32,     "FlashWindow",                          0x5745F8, &FncPtr_FlashWindow),
    DllHook(str_user32,     "GetAsyncKeyState",                     0x5745FC, &FncPtr_GetAsyncKeyState),
    DllHook(str_user32,     "GetCursorPos",                         0x574600, &FncPtr_GetCursorPos),
    DllHook(str_user32,     "GetForegroundWindow",                  0x574604, &FncPtr_GetForegroundWindow),
    DllHook(str_user32,     "GetMessageA",                          0x574608, &FncPtr_GetMessageA),
    DllHook(str_user32,     "GetSystemMetrics",                     0x57460C, &FncPtr_GetSystemMetrics),
    DllHook(str_user32,     "IsIconic",                             0x574610, &FncPtr_IsIconic),
    DllHook(str_user32,     "LoadCursorA",                          0x574614, &FncPtr_LoadCursorA),
    DllHook(str_user32,     "LoadIconA",                            0x574618, &FncPtr_LoadIconA),
    DllHook(str_user32,     "MessageBeep",                          0x57461C, &FncPtr_MessageBeep),
    DllHook(str_user32,     "MessageBoxA",                          0x574620, &FncPtr_MessageBoxA),
    DllHook(str_user32,     "OffsetRect",                           0x574624, &FncPtr_OffsetRect),
    DllHook(str_user32,     "PeekMessageA",                         0x574628, &FncPtr_PeekMessageA),
    DllHook(str_user32,     "PostMessageA",                         0x57462C, &FncPtr_PostMessageA),
    DllHook(str_user32,     "PostQuitMessage",                      0x574630, &FncPtr_PostQuitMessage),
    DllHook(str_user32,     "RegisterClassA",                       0x574634, &FncPtr_RegisterClassA),
    DllHook(str_user32,     "RegisterWindowMessageA",               0x574638, &FncPtr_RegisterWindowMessageA),
    DllHook(str_user32,     "SendMessageA",                         0x57463C, &FncPtr_SendMessageA),
    DllHook(str_user32,     "SetCursorPos",                         0x574640, &FncPtr_SetCursorPos),
    DllHook(str_user32,     "SetFocus",                             0x574644, &FncPtr_SetFocus),
    DllHook(str_user32,     "ShowCursor",                           0x574648, &FncPtr_ShowCursor),
    DllHook(str_user32,     "ShowWindow",                           0x57464C, &FncPtr_ShowWindow),
    DllHook(str_user32,     "TranslateMessage",                     0x574650, &FncPtr_TranslateMessage),
    DllHook(str_user32,     "WaitMessage",                          0x574654, &FncPtr_WaitMessage),
    DllHook(str_mpr97,      "MPRFreeStateID",                       0x57465C, &FncPtr_MPRFreeStateID),
    DllHook(str_mpr97,      "MPRStoreStateID",                      0x574660, &FncPtr_MPRStoreStateID),
    DllHook(str_mpr97,      "MPRMessage",                           0x574664, &FncPtr_MPRMessage),
    DllHook(str_mpr97,      "MPRFreeTexture",                       0x574668, &FncPtr_MPRFreeTexture),
    DllHook(str_mpr97,      "MPRAttachTexturePalette",              0x57466C, &FncPtr_MPRAttachTexturePalette),
    DllHook(str_mpr97,      "MPRLoadTexture",                       0x574670, &FncPtr_MPRLoadTexture),
    DllHook(str_mpr97,      "MPRNewTexture",                        0x574674, &FncPtr_MPRNewTexture),
    DllHook(str_mpr97,      "MPRFreeTexturePalette",                0x574678, &FncPtr_MPRFreeTexturePalette),
    DllHook(str_mpr97,      "MPRLoadTexturePalette",                0x57467C, &FncPtr_MPRLoadTexturePalette),
    DllHook(str_mpr97,      "MPRNewTexturePalette",                 0x574680, &FncPtr_MPRNewTexturePalette),
    DllHook(str_mpr97,      "MPRAttachSurface",                     0x574684, &FncPtr_MPRAttachSurface),
    DllHook(str_mpr97,      "MPRCreateRC",                          0x574688, &FncPtr_MPRCreateRC),
    DllHook(str_mpr97,      "MPRCloseDevice",                       0x57468C, &FncPtr_MPRCloseDevice),
    DllHook(str_mpr97,      "MPRDeleteRC",                          0x574690, &FncPtr_MPRDeleteRC),
    DllHook(str_mpr97,      "MPROpenDevice",                        0x574694, &FncPtr_MPROpenDevice),
    DllHook(str_mpr97,      "MPRSwapBuffers",                       0x574698, &FncPtr_MPRSwapBuffers),
    DllHook(str_mpr97,      "MPRBltFill",                           0x57469C, &FncPtr_MPRBltFill),
    DllHook(str_mpr97,      "MPRBltTransparent",                    0x5746A0, &FncPtr_MPRBltTransparent),
    DllHook(str_mpr97,      "MPRBlt",                               0x5746A4, &FncPtr_MPRBlt),
    DllHook(str_mpr97,      "MPRCreateSurface",                     0x5746A8, &FncPtr_MPRCreateSurface),
    DllHook(str_mpr97,      "MPRGetBackbuffer",                     0x5746AC, &FncPtr_MPRGetBackbuffer),
    DllHook(str_mpr97,      "MPRGetDeviceName",                     0x5746B0, &FncPtr_MPRGetDeviceName),
    DllHook(str_mpr97,      "MPRGetDeviceResolution",               0x5746B4, &FncPtr_MPRGetDeviceResolution),
    DllHook(str_mpr97,      "MPRGetDriverName",                     0x5746B8, &FncPtr_MPRGetDriverName),
    DllHook(str_mpr97,      "MPRGetSurfaceDescription",             0x5746BC, &FncPtr_MPRGetSurfaceDescription),

    // By MPRInitialize the ddraw.dll library is loaded and the DxWnd proxy import table hookings are installed.
    // To detect and manually handle the import table updates, the call can be wrapped by some own routine.
    DllHook(str_mpr97,      "MPRInitialize",                        0x5746C0, &FncPtr_MPRInitialize),

    DllHook(str_mpr97,      "MPRLockSurface",                       0x5746C4, &FncPtr_MPRLockSurface),
    DllHook(str_mpr97,      "MPRReleaseSurface",                    0x5746C8, &FncPtr_MPRReleaseSurface),
    DllHook(str_mpr97,      "MPRRestoreSurface",                    0x5746CC, &FncPtr_MPRRestoreSurface),
    DllHook(str_mpr97,      "MPRSetChromaKey",                      0x5746D0, &FncPtr_MPRSetChromaKey),
    DllHook(str_mpr97,      "MPRTerminate",                         0x5746D4, &FncPtr_MPRTerminate),
    DllHook(str_mpr97,      "MPRUnlockSurface",                     0x5746D8, &FncPtr_MPRUnlockSurface),
};

} // namespace TrekWrapper

#endif // TREKWRAPPER_TREKIMPORTTABLE_H
