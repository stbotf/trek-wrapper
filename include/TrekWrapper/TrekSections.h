#ifndef TREKWRAPPER_TREKSECTIONS_H
#define TREKWRAPPER_TREKSECTIONS_H

#ifndef WIN32_LEAN_AND_MEAN
    #define WIN32_LEAN_AND_MEAN
#endif
#include <windows.h>

namespace TrekWrapper {

// PE header offset
static const size_t TrekPeHdrOffset = 0x80;
static const size_t TrekOptHdrOffset = TrekPeHdrOffset + 0x18;
static const size_t TrekSecHdrOffset = TrekOptHdrOffset + IMAGE_SIZEOF_NT_OPTIONAL32_HEADER;

// PE header addresses
static const size_t TrekBaseAddress = 0x400000;
static const size_t TrekPeHdrAddress = TrekBaseAddress + TrekPeHdrOffset;
static const size_t TrekOptHdrAddress = TrekBaseAddress + TrekOptHdrOffset;
static const size_t TrekSecHdrAddress = TrekBaseAddress + TrekSecHdrOffset;

// section sizes
static const size_t TrekFileHeaderSize      = 0x400;
static const size_t TrekCodeSize            = 0x172600;     // .trekExe file section 1
static const size_t TrekImportSize          = 0x1400;       // .trekImp file section 2
static const size_t TrekStrSize             = 0x2B600;      // .trekStr file section 3
static const size_t TrekBssSize             = 0xE7800;      // .trekBss stripped section 4
static const size_t TrekDataSize            = 0x13A00;      // .trekDat file section 5
static const size_t TrekRelocSize           = 0x13000;      // orig .reloc file section 5
static const size_t TrekRsrcSize            = 0xA00;        // orig .rsrc file section 5
static const size_t TrekExtSize             = 0x20000;      // .extExe
static const size_t TrekMemSize             = 0x1000;       // .extMem

// asm section offsets / virtual addresses
static const size_t TrekCodeOffset          = 0x1000;       // .trekExe file section 1
static const size_t TrekImportOffset        = 0x174000;     // .trekImp file section 2
static const size_t TrekStrOffset           = 0x176000;     // .trekStr file section 3
static const size_t TrekBssOffset           = 0x1A2000;     // .trekBss stripped section 4
static const size_t TrekDataOffset          = 0x28A000;     // .trekDat file section 5
static const size_t TrekRelocOffset         = 0x28A000;     // orig .reloc file section 5
static const size_t TrekRsrcOffset          = 0x29D000;     // orig .rsrc file section 5
static const size_t TrekEndOffset           = 0x29DA00;
static const size_t TrekExtOffset           = 0x29E000;     // .extExe
static const size_t TrekMemOffset           = 0x2BE000;     // .extMem

// asm section asdresses
static const size_t TrekCodeAddress         = TrekBaseAddress + TrekCodeOffset;         // .trekExe file section 1
static const size_t TrekImportAddress       = TrekBaseAddress + TrekImportOffset;       // .trekImp file section 2
static const size_t TrekStrAddress          = TrekBaseAddress + TrekStrOffset;          // .trekStr file section 3
static const size_t TrekBssAddress          = TrekBaseAddress + TrekBssOffset;          // .trekBss stripped section 4
static const size_t TrekDataAddress         = TrekBaseAddress + TrekDataOffset;         // .trekDat file section 5
static const size_t TrekEndAddress          = TrekBaseAddress + TrekEndOffset;
static const size_t TrekExtAddress          = TrekBaseAddress + TrekExtOffset;          // .extExe
static const size_t TrekMemAddress          = TrekBaseAddress + TrekMemOffset;          // .extMem

// file section addresses
static const size_t TrekFileCodeAddress     = TrekFileHeaderSize;                       // 0x400    .trekExe file section 1
static const size_t TrekFileImportAddress   = TrekFileCodeAddress + TrekCodeSize;       // 0x172A00 .trekImp file section 2
static const size_t TrekFileStrAddress      = TrekFileImportAddress + TrekImportSize;   // 0x173E00 .trekStr file section 3
static const size_t TrekFileDataAddress     = TrekFileStrAddress + TrekStrSize;         // 0x19F400 .trekDat file section 5
static const size_t TrekFileEndAddress      = TrekFileDataAddress + TrekDataSize;       // 0x1B2E00
static const size_t TrekFileExtAddress      = TrekFileEndAddress;                       // 0x1B2E00 .extExe
static const size_t TrekFileMemAddress      = TrekFileExtAddress + TrekExtSize;         // 0x1D2E00 .extMem

} // namespace TrekWrapper

#endif // TREKWRAPPER_TREKSECTIONS_H
